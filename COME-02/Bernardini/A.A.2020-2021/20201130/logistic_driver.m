%
% display of the logistic function
%
L = [1, 1.5, 2]; % maximum value of the curve
x0 = [0, 5 15]; % center of the sigmoid
k = [3, 1, 0.1]; % steepness of the sigmoid
x = [-30:0.01:60];
%
% here's the logistic function
%
y0 = logistic(x, L(1), x0(1), k(1));
y1 = logistic(x, L(2), x0(2), k(2));
y2 = logistic(x, L(3), x0(3), k(3));

F0 = figure(1);
title("Funzione Logistica")
plot(x, y0, [";k = " num2str(k(1)) ";"], "linewidth", 4, x, y1, [";k = " num2str(k(2)) ";"], "linewidth", 4, x, y2, [";k = " num2str(k(3)) ";"], "linewidth", 4);
axis([x(1) x(end) -0.2 max(L)+0.5])

iterations = 100;
start_value = 0.4;
r = [1.2 2.6 3.1 3.7];
yd = zeros(length(r), iterations);

for k=1:length(r)
  yd(k,:) = logistic_map(start_value, r(k), iterations);
end

F1 = figure(2);
plot(yd(1,:), [";r = " num2str(r(1)) ";"], "linewidth", 2, yd(2,:), [";r = " num2str(r(2)) ";"], "linewidth", 2, yd(3,:), [";r = " num2str(r(3)) ";"], "linewidth", 2, yd(4,:), [";r = " num2str(r(4)) ";"], "linewidth", 2);
axis([0 40 -0.05 1.3])
