## Copyright (C) 2020 Nicola Bernardini
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{y} =} logistic_map (@var{s}, @var{r}, @var{iter})
##
## where:
## @var(s) is the starting point
## @var(r) is the reproduction factor
## @var(iter) is the number of iterations
##
## @seealso{}
## @end deftypefn

## Author: Nicola Bernardini <nicb@nicb-p302u>
## Created: 2020-11-30

function y = logistic_map(s, r, its)
  y = zeros(1, its);
  y(1) = s;
  for k=2:its
    y(k) = (r*y(k-1))*(1-(y(k-1)));
  end
  y;
endfunction
