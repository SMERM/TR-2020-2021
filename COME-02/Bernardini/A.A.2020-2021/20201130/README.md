# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 30/11/2020

<!-- ## [Youtube video](https://youtu.be/kCq-hUzSqnQ) -->

### Verifica dello studio a casa

* verifica consegne

### Strategie di organizzazione parametriche

#### Organizzazioni casuali

* principi di casualità ![uniform and gauss](./uniform_gauss.png)
  * distribuzione uniforme delimitata
    * delimitazioni costanti
    * delimitazioni dinamiche tempo-dipendenti
      * funzioni lineari
      * funzioni esponenziali
      * funzioni polinomiali
  * distribuzione gaussiana
    * `random.gauss(mu, sigma)`
    * utilizzazione dinamica dei parametri
  * distribuzioni caotiche
    * funzione logistica
    * *logistic mapping* ![logistic function and map](./logistic_function_and_map.png)

##### Funzioni logistiche in `octave`

* [funzione logistica](./logistic.m)

```matlab
function y = logistic (x, L, x0, k)
  y = L ./ (1 + exp(-k*(x-x0))); 
endfunction
```

* [mappa logistica](./logistic_map.m)

```matlab
function y = logistic_map(r, iterations)
  y = zeros(1, iterations);
  y(1) = 1;
  for k=2:iterations
    y(k) = (r*y(k-1))*(1-(y(k-1)));
  end
  y;
endfunction
```

È possibile verificare/sperimentare queste due funzioni [con uno script
`matlab` quale ad esempio](./logistic_driver.m):

```matlab
%
% display of the logistic function
%
L = [1, 1.5, 2]; % maximum value of the curve
x0 = [0, 5 15]; % center of the sigmoid
k = [3, 1, 0.1]; % steepness of the sigmoid
x = [-30:0.01:60];
%
% here's the logistic function
%
y0 = logistic(x, L(1), x0(1), k(1));
y1 = logistic(x, L(2), x0(2), k(2));
y2 = logistic(x, L(3), x0(3), k(3));

F0 = figure(1);
title("Funzione Logistica")
plot(x, y0, [";k = " num2str(k(1)) ";"], "linewidth", 4, x, y1, [";k = " num2str(k(2)) ";"], "linewidth", 4, x, y2, [";k = " num2str(k(3)) ";"], "linewidth", 4);
axis([x(1) x(end) -0.2 max(L)+0.5])

iterations = 100;
start_value = 0.4;
r = [1.2 2.6 3.1 3.7];
yd = zeros(length(r), iterations);

for k=1:length(r)
  yd(k,:) = logistic_map(start_value, r(k), iterations);
end

F1 = figure(2);
plot(yd(1,:), [";r = " num2str(r(1)) ";"], "linewidth", 2, yd(2,:), [";r = " num2str(r(2)) ";"], "linewidth", 2, yd(3,:), [";r = " num2str(r(3)) ";"], "linewidth", 2, yd(4,:), [";r = " num2str(r(4)) ";"], "linewidth", 2);
axis([0 40 -0.05 1.3])
```
che produce i grafici seguenti:

![logistic_function](./logistic_function.png)

![logistic_map](./logistic_map.png)

### Studio a casa

* continuazione del capitolo 4 del [FLOSS manual di `csound`](https://flossmanual.csound.com/sound-synthesis/additive-synthesis)
* continuazione della lettura dei tutorials (facendo suonare gli esempi) di `SuperCollider`
* completare i due esercizi di csound:
  * esercizio 1: frammento di ca. 1 minuto con cross-fades *à la Bergman* di quattro suoni concreti (tratti da [`freesound.org`](https://freesound.org) oppure da una biblioteca personale)
  * esercizio 2: frammento di ca. 1 minuto con un migliaio di brevissimi frammenti raccolti e montati in forma algoritmica usando la API di [`freesound.org`](https://freesound.org) interpolati da sinusoidi
