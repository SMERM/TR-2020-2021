from frammento import Frammento
from sequenza import Sequenza

frags = [
        Frammento(1, -3, 58.703),
        Frammento(2, -3, 16.129),
        Frammento(3, +3, 306.397),
        Frammento(4, +3, 1611.573),
]
ritmi = [1/8.0, 1/16.0, 1/16.0, 1/2.0, 1/8.0, 1/24.0, 1/24.0, 1/24.0]

s = Sequenza(0, 60.0, 112.0, 184.0, frags, ritmi)

print("f1 0 16384 20 2 1")
s.run()
