from frammento import Frammento
from linear import Linear

class Sequenza:
    def __init__(self, at, dur, metros, metroe, frags, rhythms):
        self.at = at
        self.dur = dur
        self.frammenti = frags
        self.ritmi = rhythms
        self.r_index = 0
        self.r_frag = 0
        self.metro_start = metros
        self.metro_end = metroe
        self.metro_fun = Linear(self.at, self.metro_start, self.at + self.dur, self.metro_end)
    
    def run(self):
        now = self.at
        endt = self.at + self.dur
        while(now < endt):
            step = self.next_rhythm(now)
            frag = self.next_frammento()
            skip = now % frag.dur
            print("i2 %8.4f %8.4f %d %8.4f %+5.2f" % (now, step, frag.number, skip, frag.amp)) 
            now += step

    def next_rhythm(self, t):
        periodo = 60.0/self.metro_fun.y(t)
        res = 4*self.ritmi[self.r_index]*periodo
        self.r_index += 1
        self.r_index %= len(self.ritmi)
        return res

    def next_frammento(self):
        res = self.frammenti[self.r_frag]
        self.r_frag += 1
        self.r_frag %= len(self.frammenti)
        return res
