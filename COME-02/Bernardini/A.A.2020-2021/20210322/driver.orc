sr = 44100
kspms = 5
nchnls = 2
0dbfs = 1


        instr 1
ifile   = p4
iskip   = p5
iamp    = ampdbfs(p6)
idur    = p3

kenv    oscil     iamp, 1/idur, 1
aL, aR  diskin2   ifile, 1, iskip, 1

        outs  aL*kenv, aR*kenv

        endin

        instr 2
ifile   = p4
iskip   = p5
iamp    = ampdbfs(p6)
idur    = p3

kenv    linen     iamp, 0.01*idur, idur, 0.01*idur
aL, aR  diskin2   ifile, 1, iskip, 1

        outs  aL*kenv, aR*kenv

        endin
