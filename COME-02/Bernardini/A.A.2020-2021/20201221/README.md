# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 21/12/2020

## [Youtube video](https://youtu.be/2aXS4HJJ4lc)

### Verifica dello studio a casa

* verifica consegne
* correzione delle consegne

## Correzione delle consegne

Vengono fatte alcune considerazioni qui riportate graficamente:

![disegno 1](./possibili_andamenti.png)

```python
from forma import forma
from contenitore import contenitore
from logfun import logfun

bass = [
    forma(0, 7, 50, 150, 400),
    forma(14, 21, 50, 150, 400),
    forma(28, 35, 50, 150, 400),
    forma(42, 49, 50, 150, 400),
    forma(56, 63, 50, 150, 400),
]

soprano = contenitore(5, 55, 1500, 500, 300, 7)

print("f1 0 8192 10 1")

nnotes = 100

for t in bass:
    durrange = t.x2 - t.x0
    startstep = (durrange * 0.1)/(nnotes * 0.6)
    endstep = (durrange * 0.9)/(nnotes * 0.4)
    at = t.x0
    et = t.x2
    while(at <= et):
        t.to_csound(at, 0.1, -30)
        step = logfun(at, t.x0, startstep, et, endstep)
        at += step

soprano.to_csound(nnotes)
```

Questo programma importa i seguenti oggetti:

```python
from triangolo_base import Triangolouno


class forma(Triangolouno):

    def __init__(self, at, et, botf, topf, finalf):
        super().__init__(at, botf, topf, et, finalf)

    def to_csound(self, t, dur, amp):
        freq = self.choose_y(t)
        cs_string = "i1 %8.4f %8.4f %+5.2f %8.4f" % (t, dur, amp, freq)
        print(cs_string)
```

```python
from linear import linear
from forma import forma

class contenitore:

    def __init__(self, at, dur, f0, f1, f0range, n):
        self.at = at
        self.dur = dur
        self.f0 = f0
        self.f1 = f1
        self.f0range = f0range
        self.n = n
        self.triangoli = self.setup()

    def setup(self):
        res = []
        cur = 0
        tridur = self.dur / (self.n*2-1)
        step = tridur * 2
        initial_at = at = self.at
        while(cur < self.n):
           et = at + tridur
           f0center_start = linear(at, initial_at, self.f0, et, self.f1)
           f0center_end = linear(et, initial_at, self.f0, et, self.f1)
           topf = f0center_start + (0.5*self.f0range)
           botf = f0center_start - (0.5*self.f0range)
           res.append(forma(at, et, topf, botf, f0center_end))
           cur += 1
           at += step
        return res

    def to_csound(self, nnote):
        for t in self.triangoli:
            at = t.x0
            et = t.x2
            dur = et - at
            step = dur / (nnote - 1)
            notedur = step/2.0
            while(at <= et):
                t.to_csound(at, notedur, -30)
                at += step
```

Vegono realizzate le funzioni `expfun` e `logfun`

![funzioni](./funzioni.png)

```python
from math import exp, log

def logfun (x, x0, y0, x1, y1):
    a = (exp(y1) - exp(y0)) / (x1 - x0)
    b = -(a * (x0)) + exp(y0)
    return log(x * a + b)
```

```python
from math import exp, log

def expfun (x, x0, y0, x1, y1):
    a = (log(y1) - log(y0)) / (x1 - x0)
    b = -(a * (x0)) + log(y0)
    return exp(x * a + b)
```

Eseguendo poi:

```python
$ python finale.py > score.sco
$ csound driver.orc score.sco
```

si ottiene il risultato che è possibile verificare nel seguente spettrogramma:

![spectrogram](./output-spectrogram.png)
