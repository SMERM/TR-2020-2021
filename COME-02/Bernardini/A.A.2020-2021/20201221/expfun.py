from math import exp, log

def expfun (x, x0, y0, x1, y1):
    a = (log(y1) - log(y0)) / (x1 - x0)
    b = -(a * (x0)) + log(y0)
    return exp(x * a + b)
