from math import exp, log

def logfun (x, x0, y0, x1, y1):
    a = (exp(y1) - exp(y0)) / (x1 - x0)
    b = -(a * (x0)) + exp(y0)
    return log(x * a + b)
