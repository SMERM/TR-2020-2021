from forma import forma
from contenitore import contenitore
from logfun import logfun

bass = [
    forma(0, 7, 50, 150, 400),
    forma(14, 21, 50, 150, 400),
    forma(28, 35, 50, 150, 400),
    forma(42, 49, 50, 150, 400),
    forma(56, 63, 50, 150, 400),
]

soprano = contenitore(5, 55, 1500, 500, 300, 7)

print("f1 0 8192 10 1")

nnotes = 100

for t in bass:
    durrange = t.x2 - t.x0
    startstep = (durrange * 0.1)/(nnotes * 0.6)
    endstep = (durrange * 0.9)/(nnotes * 0.4)
    at = t.x0
    et = t.x2
    while(at <= et):
        t.to_csound(at, 0.1, -30)
        step = logfun(at, t.x0, startstep, et, endstep)
        at += step

soprano.to_csound(nnotes)
