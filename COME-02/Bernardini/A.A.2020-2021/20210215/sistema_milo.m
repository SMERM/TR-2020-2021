
DO=261.63;
scala_temperata = DO*(2.**([0:11]/12));

base_replica=5/2;
nnote=4;
note = [-nnote*2:nnote*2];

frequenze = DO*(base_replica.**(note/nnote));
frequenze_complete = zeros(1, length(frequenze)*3);

idx = 1;
for k=1:length(frequenze)
  inf = frequenze(k)*2**(-1/24);
  sup = frequenze(k)*2**(+1/24);
  frequenze_complete(idx) = inf;
  frequenze_complete(idx+1) = frequenze(k);
  frequenze_complete(idx+2) = sup;
  idx += 3;
end
