clear -a;
sinc = 0.01;
metro = 96;
tstart = -5;
tend = 15;
t = [tstart:sinc:tend-sinc];

periodo = 3;
halfperiodo = periodo/2;
freq = 1/periodo;
tmetro = [0:sinc:halfperiodo];
metrofun1 = metro + (90*sin(2*pi*freq*tmetro));
metrofun2 = metro - (45*sin(2*pi*freq*tmetro));
metroconstant = ones(1, halfperiodo/sinc)*metro;

metro1 = [ metroconstant metrofun1 metroconstant metrofun1 metroconstant metrofun1 metroconstant metrofun1 metroconstant metrofun1 metroconstant ];
metro2 = [ metroconstant metrofun2 metroconstant metrofun2 metroconstant metrofun2 metroconstant metrofun2 metroconstant metrofun2 metroconstant ];

plot(t(1:length(metro1)), metro1, t(1:length(metro2)), metro2)
print -dpng "metronomi_diversi_andamento.png"

pulsazione1(:,1) = t(1:length(metro1))';
pulsazione1(:,2) = (60./metro1)';
pulsazione2(:,1) = t(1:length(metro2))';
pulsazione2(:,2) = (60./metro2)';

function v = pulse_lookup(t, arr)
  idx = lookup(arr(:,1), t);
  v = arr(idx,2);
end

pulse1 = [];
pulse2 = [];

tnow = tstart;
while (tnow < tend)
   pulse1 = horzcat(pulse1, [ tnow ]);
   tnow += pulse_lookup(tnow, pulsazione1);       % calcola la pulsazione al tempo tnow
end

tnow = tstart;
while (tnow < tend)
   pulse2 = horzcat(pulse2, [ tnow ]);
   tnow += pulse_lookup(tnow, pulsazione2);       % calcola la pulsazione al tempo tnow
end

plot(pulse1, ones(1, length(pulse1)), '+', pulse2, ones(1, length(pulse2)) * 0.5, '+')
set(gca, "xgrid", "on")
axis([tstart tend 0 1.2], "nolabel", "labelx")
print -dpng "metronomi_diversi_pulsazioni.png"
