# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 15/02/2021

## [Youtube video](https://youtu.be/NHdvrqpQbyc)

### Verifica dello studio a casa

* verifica consegne: progetto "canone a tre voci" (Sanfeliz, Putelli, De Mattei)

### Organizzazione temporale degli eventi

* pulsazione
* tempo
  * conversione metronomo -> tempo lineare

### Realizzazione del `sistema Milo`

Per esemplificare un possibile ordine strutturale diverso dal sistema
temperato equabile abbiamo realizzato un sistema denominato *sistema Milo*

* Il `sistema Milo` è un esempio di ordine strutturale delle frequenze diverso
  dal temperamento cromatico dodecafonico equabile
* il sistema è basato su un modulo sonoro che si ripete identico ogni
  intervallo 5/2 (ossia un'ottava più una terza naturale)
* all'interno di questo intervallo di 5/2 vi sono quattro note "polari* disposte
  secondo un temperamento equabile
* intorno a queste note polari ci sono due note d'appoggio: una un quarto di
  tono (dodecafonico temperato) sopra e una un quarto di tono (dodecafonico
  temperato) sotto

La segmentazione dello spazio frequenziale è schematizzata così:

![sistema milo - schema](./sistema_milo.png)

Rappresentandola attraverso uno *script* `octave`:

```matlab
DO=261.63;
scala_temperata = DO*(2.**([0:11]/12));

base_replica=5/2;
nnote=4;
note = [-nnote*2:nnote*2];

frequenze = DO*(base_replica.**(note/nnote));
frequenze_complete = zeros(1, length(frequenze)*3);

idx = 1;
for k=1:length(frequenze)
  inf = frequenze(k)*2**(-1/24);
  sup = frequenze(k)*2**(+1/24);
  frequenze_complete(idx) = inf;
  frequenze_complete(idx+1) = frequenze(k);
  frequenze_complete(idx+2) = sup;
  idx += 3;
end
```
i grafici risultati sono:

![note "polari"](./sistema_milo_frequenze_polari.png)
**Note polari**

![note "polari" e appoggiature](./sistema_milo_tutte_le_frequenze.png)
**Note polari e appoggiature**

Per ascoltarle, abbiamo realizzato un [patch `pure data`](./sistema_milo.pd)

![`sistema_milo.pd`](./sistema_milo_pd_patch.png)

## Organizzazione temporale degli eventi

### Conversione metro/tempo cronometrico

```matlab
spm = 60.0;
metronomo=[30:0.5:200];
periodo_metronomico = spm./metronomo;
plot(metronomo, periodo_metronomico);
```

![metro -> tempo cronometrico](./metro_tempo.png)

### Metronomi concorrenti variabili

Elaborazione di due voci a due tempi metronomici variabili diversi.
Profilazione in octave:

```matlab
clear -a;
sinc = 0.01;
metro = 96;
tstart = -5;
tend = 15;
t = [tstart:sinc:tend-sinc];

periodo = 3;
halfperiodo = periodo/2;
freq = 1/periodo;
tmetro = [0:sinc:halfperiodo];
metrofun1 = metro + (90*sin(2*pi*freq*tmetro));
metrofun2 = metro - (45*sin(2*pi*freq*tmetro));
metroconstant = ones(1, halfperiodo/sinc)*metro;

metro1 = [ metroconstant metrofun1 metroconstant metrofun1 metroconstant metrofun1 metroconstant metrofun1 metroconstant metrofun1 metroconstant ];
metro2 = [ metroconstant metrofun2 metroconstant metrofun2 metroconstant metrofun2 metroconstant metrofun2 metroconstant metrofun2 metroconstant ];

plot(t(1:length(metro1)), metro1, t(1:length(metro2)), metro2)

pulsazione1(:,1) = t(1:length(metro1))';
pulsazione1(:,2) = (60./metro1)';
pulsazione2(:,1) = t(1:length(metro2))';
pulsazione2(:,2) = (60./metro2)';

function v = pulse_lookup(t, arr)
  idx = lookup(arr(:,1), t);
  v = arr(idx,2);
end

pulse1 = [];
pulse2 = [];

tnow = tstart;
while (tnow < tend)
   pulse1 = horzcat(pulse1, [ tnow ]);
   tnow += pulse_lookup(tnow, pulsazione1);       % calcola la pulsazione al tempo tnow
end

tnow = tstart;
while (tnow < tend)
   pulse2 = horzcat(pulse2, [ tnow ]);
   tnow += pulse_lookup(tnow, pulsazione2);       % calcola la pulsazione al tempo tnow
end

plot(pulse1, ones(1, length(pulse1)), '+', pulse2, ones(1, length(pulse2)) * 0.5, '+')
set(gca, "xgrid", "on")
axis([tstart tend 0 1.2], "nolabel", "labelx")
```

Questo script produce:

il grafico dell'andamento dei metronomi concorrenti:
![andamento](./metronomi_diversi_andamento.png)

e il grafico delle pulsazioni metronomiche relative:
![pulsazioni](./metronomi_diversi_pulsazioni.png)
