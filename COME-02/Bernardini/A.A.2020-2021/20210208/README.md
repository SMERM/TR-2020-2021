# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 08/02/2021

<!-- ## [Youtube video](https://youtu.be/HPuNbEKujTY) -->

### Verifica dello studio a casa

* verifica consegne: progetto "canone a tre voci"

### SuperCollider

* Architettura di `SuperCollider`:
  * libreria
* Hands-on:
  * `Pbind` e `Pseq`
  * sistemi d'intonazione diversi da quello cromatico temperato dodecafonico
```SCLang
//
// transpose without mtranspose
//
(
  a = [0, 1, 2, 3, 4, 11];
  a.postln;
  ((a - 4) % 12).postln; // % 12 serve per fare tutte le somme modulo 12
)

//
// sistema di intonazione diverso
//
(
   var do = 261.63;
   var baseref = 7/2;
   var nnote = 9;

   var scala = { arg brf, notestart, nn, values; do*(brf**(values/nn)) };
   var s_temp = scala.value(2, do, 12, Interval(0,11).as(Array));

   var risultato = scala.value(baseref, do, nnote, Interval(0, nnote-1).as(Array));
   // a SynthDef
   (
      SynthDef(\test, { | out, freq = 440.0, amp = 0.1, nharms = 3, pan = 0, gate = 1 |
      var audio = Blip.ar(freq, nharms, amp);
      var env = Linen.kr(gate, doneAction: Done.freeSelf);
      OffsetOut.ar(out, Pan2.ar(audio, pan, env) );
     }).add;



Pbind(\instrument, \test,
	\freq, Pseq(risultato, inf),
	\dur, Pseq([ 0.5 ], inf)
  ).play(TempoClock(141/60));
)
)


//
// dalla documentazione di PBind
//
(
// a SynthDef
SynthDef(\test, { | out, freq = 440, amp = 0.1, nharms = 10, pan = 0, gate = 1 |
    var audio = Blip.ar(freq, nharms, amp);
    var env = Linen.kr(gate, doneAction: Done.freeSelf);
    OffsetOut.ar(out, Pan2.ar(audio, pan, env) );
}).add;


Pbind(\instrument, \test, \freq, Prand([1, 1.2, 2, 2.5, 3, 4], inf) * 400, \dur, 0.1).play;
)
```
