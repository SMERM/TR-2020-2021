# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 19/04/2021

## [YouTube Video](./https://youtu.be/dp33O830z2c)

## Discussione dei progetti di elaborato degli studenti

### Discussione dell'elaborato di Leonardo Polla

### [Discussione dell'elaborato di Sheila Sanfeliz](./sheila/README.md)

![relazioni di fase](./phase_relation.png)
