class Linea:

    def __init__(self, at, dur, fp, fa, amp):
        self.at = at
        self.dur = dur
        self.frequenza_partenza = fp
        self.frequenza_arrivo = fa
        self.amp = amp

    def to_csound(self):
        print("i1 %8.4f %8.4f %+5.2f %12.8f %12.8f" % (self.at, self.dur, self.amp, self.frequenza_partenza, self.frequenza_arrivo))
