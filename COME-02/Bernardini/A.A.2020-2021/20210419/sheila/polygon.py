from linea import Linea

class Polygon:

    def __init__(self, nome, conf):
        self.nome   = nome
        self.vertici = conf['vertici']
        self.colore = conf['colore']
        self.amp    = conf['amp']
        self.setup()

    def setup(self):
        self.linee  = []
        self.calc_linee()

    def to_csound(self):
        print("; %s" % (self.nome))
        for v in self.vertici:
            print(";\tx: %8.4f y: %8.4f" % (v[0], v[1]))
        for l in self.linee:
            l.to_csound()


    def calc_linee(self):
        #
        # questo metodo deve calcolare tutti i glissandi sinusoidali
        #
        pass
