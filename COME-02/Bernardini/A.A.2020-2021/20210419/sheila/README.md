# 19/04/2021 - Problematiche legate alla composizione di Sheila Sanfeliz

Note sinusoidali glissate circoscritte in poligoni

![poligoni](./polygons.png)

## [Meta partitura](./metascore.yaml)

```yaml
triangolo_1:
  vertici: [[0.1, 235], [0.3, 600], [0.3, 80]]
  colore: blu
  amp:    -12
  vertice_iniziale: 0

triangolo_2:
  vertici: [[0.3, 800], [0.3, 410], [0.7, 550]]
  colore: marrone
  amp:    -16
  vertice_iniziale: [0, 1]
```

## Codice `python`

### [Oggetto `Linea`](./linea.py)

```python
class Linea:

    def __init__(self, at, dur, fp, fa, amp):
        self.at = at
        self.dur = dur
        self.frequenza_partenza = fp
        self.frequenza_arrivo = fa
        self.amp = amp

    def to_csound(self):
        print("i1 %8.4f %8.4f %+5.2f %12.8f %12.8f" % (self.at, self.dur, self.amp, self.frequenza_partenza, self.frequenza_arrivo))
```

### [Oggetto `Polygon`](./polygon.py)

```python
from linea import Linea

class Polygon:

    def __init__(self, nome, conf):
        self.nome   = nome
        self.vertici = conf['vertici']
        self.colore = conf['colore']
        self.amp    = conf['amp']
        self.setup()

    def setup(self):
        self.linee  = []
        self.calc_linee()

    def to_csound(self):
        print("; %s" % (self.nome))
        for v in self.vertici:
            print(";\tx: %8.4f y: %8.4f" % (v[0], v[1]))
        for l in self.linee:
            l.to_csound()


    def calc_linee(self):
        #
        # questo metodo deve calcolare tutti i glissandi sinusoidali
        #
        pass
```

### [Oggetto `MetaScore`](./metascore.py)

```python
import yaml
from polygon import Polygon

class MetaScore:

    def __init__(self, conf):
        self.poligoni = []
        self.setup(conf)

    def setup(self, conf):
        with open(conf, 'r') as f:
            self.conf = yaml.load(f)
        for nome, values in self.conf.items():
            self.poligoni.append(Polygon(nome, values))

    def to_csound(self):
        for p in self.poligoni:
            p.to_csound()
```

### [`main`](./main.py)

```python
from metascore import MetaScore

ms = MetaScore('metascore.yaml')
ms.to_csound()
```
