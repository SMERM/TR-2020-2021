import yaml
from polygon import Polygon

class MetaScore:

    def __init__(self, conf):
        self.poligoni = []
        self.setup(conf)

    def setup(self, conf):
        with open(conf, 'r') as f:
            self.conf = yaml.load(f)
        for nome, values in self.conf.items():
            self.poligoni.append(Polygon(nome, values))

    def to_csound(self):
        for p in self.poligoni:
            p.to_csound()
