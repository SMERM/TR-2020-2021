from random import random

print("f1 0 4096 10 1 0.5 0.33 0.25 0.2 0.166 0.14285714\nf2 0 4096 10 1")

totdur=60
voice1dur=totdur*(5.0/6.0)
start1=0.0
nnote1=400
dur1bot = 0.05
dur1top = 0.5
dur1range = dur1top - dur1bot

n1=0
bot1=112.0
top1=8000.0
range1=top1-bot1
while(n1 < nnote1):
    at1 = random()*voice1dur+start1
    dur1 = random()*dur1range+dur1bot
    freq = random()*range1+bot1
    print("i1 %8.4f %8.4f %8.4f" % (at1, dur1, freq))
    n1 += 1
