from random import random

print("f1 0 4096 10 1 0.5 0.33 0.25 0.2 0.166 0.14285714\nf2 0 4096 10 1")

totdur = 60

def voice(st, vdur, nnote, durmin, durmax, fmin, fmax):
    durrange = durmax - durmin
    frange = fmax - fmin
    n = 0
    while (n < nnote):
        at = random()*vdur+st
        dur = random()*durrange+durmin
        freq = random()*frange+fmin
        print("i1 %8.4f %8.4f %8.4f" % (at, dur, freq))
        n += 1

voice(0, totdur*(5.0/6.0), 400, 0.05, 0.5, 112, 1000)
voice(totdur/2.0, 2, 20, 0.1, 0.3, 5000, 8000)
