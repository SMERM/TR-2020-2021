# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Tutorial sulla [consegna n.1 del 16 novembre 2020](../../20201116/README.md#studio-a-casa)

* su richiesta degli studenti viene fatto un primo tutorial sulle
  consegne delle lezioni precedenti.
  In particolare, viene [svolta in classe la prima consegna del 16 novembre
  2020](./tutorial-esercizio_1-20201116/README.md)

### Esercizio di `csound` - consegna:

* esercizio 1: frammento di ca. 1 minuto con 4 fasce sonore continue e ben differenziate (differenziazione dinamica)
  ![esercizio n.1](../../20201116/esercizi.png)

### Esercizio `csound` n.1 - svolgimento:

* introduzione al concetto di *campionamento*
* introduzione al concetto di `k-rate` e alla *keyword* di `csound` `ksmps`
* elaborazione [dell'*orchestra `csound`* più semplice](./orchestra.orc):
  orchestra con un oscillatore tabellare e partitura scritta a mano:
```csound
sr=44100
ksmps=5
nchnls=1
0dbfs=1


             instr 1

kenv         linen    0.2,0.01*p3,p3,0.01*p3
aout         oscil    kenv, p4, 1
             out      aout

             endin
```
* introduzione alla [sintassi delle partiture `csound`](./partitura.sco)
```csound
f1 0 4096 10 1 0.5 0.33 0.25 0.2 0.166 0.14285714
f2 0 4096 10 1

i1 0 0.2 261.6
i1 0.3 0.2 446
i1 0.6 0.5 1000
```
* introduzione al concetto di inviluppo di ampiezza trapezoidale e alla sua
  implementazione in `csound`:
  ![`linen`](./linen.png)
* prima realizzazione di un [algoritmo per la generazione automatica di
  partiture scritto in `python`](./primo.py)
```python
from random import random

print("f1 0 4096 10 1 0.5 0.33 0.25 0.2 0.166 0.14285714\nf2 0 4096 10 1")

totdur=60
voice1dur=totdur*(5.0/6.0)
step1=0.15
dur1=step1*0.7

at1=0
et1=at1+voice1dur
bot1=112.0
top1=200.0
range1=top1-bot1
while(at1 < et1):
    freq = random()*range1+bot1
    print("i1 %8.4f %8.4f %8.4f" % (at1, dur1, freq))
    at1 += step1
```
  Tale algoritmo viene utilizzato dalla `consolle` del terminale così (frammento rilevante):
```bash
$ python primo.py > random0.sco
$ csound orchestra.orc random0.sco
time resolution is 1000.000 ns
0dBFS level = 32768.0
--Csound version 6.12 beta (double samples) 2019-03-21
[commit: none]
libsndfile-1.0.28
orchname:  orchestra.orc
rtaudio: ALSA module enabled
rtmidi: ALSA Raw MIDI module enabled
Elapsed time at end of orchestra compile: real: 0.025s, CPU: 0.002s
sorting score ...
	... done
Elapsed time at end of score sort: real: 0.026s, CPU: 0.003s
graphics suppressed, ascii substituted
0dBFS level = 1.0
orch now loaded
audio buffered in 256 sample-frame blocks
writing 512-byte blks of shorts to test.wav (WAV)
SECTION 1:
ftable 1:
ftable 1:	4096 points, scalemax 1.000
    -'-
   .   -
        -
  _      '_ __.-.
           '     '.
                   '.
 .                   '''''-.
                            '.
                              '-..___
_____________________________________'-________________________________________
                                        -.
                                          ''''--.
                                                 '.                           .
                                                   '-._____
                                                           -_
                                                             .     __        .
                                                              '-.-'  -_
                                                                       _    _
                                                                        _
                                                                         ._-
ftable 2:
ftable 2:	4096 points, scalemax 1.000
               .-''''''-._
            _-'           '.
          _-                '.
         -                    '_
       .'                       -
      -                          '_
    _'                             .
   .                                -
  -                                  '_
_'_____________________________________._______________________________________
                                        -                                     .
                                         -                                   -
                                          '_                               _'
                                            .                             .
                                             -                           -
                                              '.                       _'
                                                -_                    -
                                                  -_                .'
                                                    -_           _.'
                                                      '-._____..'
new alloc for instr 1:
B  0.000 ..  0.150 T  0.150 TT  0.150 M:  0.20000
B  0.150 ..  0.300 T  0.300 TT  0.300 M:  0.20000
B  0.300 ..  0.450 T  0.450 TT  0.450 M:  0.20000
B  0.450 ..  0.600 T  0.600 TT  0.600 M:  0.20000
...
... ecc. ecc.
...
B 49.800 .. 49.950 T 49.950 TT 49.950 M:  0.20000
B 49.950 .. 50.055 T 50.055 TT 50.055 M:  0.20000
Score finished in csoundPerform().
inactive allocs returned to freespace
end of score.		   overall amps:  0.20000
	   overall samples out of range:        0
0 errors in performance
Elapsed time at end of performance: real: 0.140s, CPU: 0.085s
256 512 sample blks of shorts written to test.wav (WAV)
```
  Questa esecuzione produce il file `test.wav` di cui viene qui riportato lo
  [*spettrogramma*](./esercizio_1_0-spettrogramma_0_5s.png):
  ![*spettrogramma* esercizio 1.0 (primi 5 secondi)](./esercizio_1_0-spettrogramma_0_5s.png)
* seconda iterazione di un [algoritmo per la generazione automatica di
  partiture scritto in `python`](./secondo.py) - viene introdotta la
  *randomizzazione* di *action time* e durate
```python
from random import random

print("f1 0 4096 10 1 0.5 0.33 0.25 0.2 0.166 0.14285714\nf2 0 4096 10 1")

totdur=60
voice1dur=totdur*(5.0/6.0)
start1=0.0
nnote1=400
dur1bot = 0.05
dur1top = 0.5
dur1range = dur1top - dur1bot

n1=0
bot1=112.0
top1=8000.0
range1=top1-bot1
while(n1 < nnote1):
    at1 = random()*voice1dur+start1
    dur1 = random()*dur1range+dur1bot
    freq = random()*range1+bot1
    print("i1 %8.4f %8.4f %8.4f" % (at1, dur1, freq))
    n1 += 1
```
* terza iterazione di un [algoritmo per la generazione automatica di
  partiture scritto in `python`](./terzo.py) - viene introdotto il
  concetto di *funzione*:
```python
from random import random

print("f1 0 4096 10 1 0.5 0.33 0.25 0.2 0.166 0.14285714\nf2 0 4096 10 1")

totdur = 60

def voice(st, vdur, nnote, durmin, durmax, fmin, fmax):
    durrange = durmax - durmin
    frange = fmax - fmin
    n = 0
    while (n < nnote):
        at = random()*vdur+st
        dur = random()*durrange+durmin
        freq = random()*frange+fmin
        print("i1 %8.4f %8.4f %8.4f" % (at, dur, freq))
        n += 1

voice(0, totdur*(5.0/6.0), 400, 0.05, 0.5, 112, 1000)
voice(totdur/2.0, 2, 20, 0.1, 0.3, 5000, 8000)
```
