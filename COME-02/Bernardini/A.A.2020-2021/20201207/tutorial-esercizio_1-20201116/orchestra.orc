sr=44100
ksmps=5
nchnls=1
0dbfs=1


             instr 1

kenv         linen    0.2,0.01*p3,p3,0.01*p3
aout         oscil    kenv, p4, 1
             out      aout

             endin
