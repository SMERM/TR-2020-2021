from random import random

print("f1 0 4096 10 1 0.5 0.33 0.25 0.2 0.166 0.14285714\nf2 0 4096 10 1")

totdur=60
voice1dur=totdur*(5.0/6.0)
step1=0.15
dur1=step1*0.7

at1=0
et1=at1+voice1dur
bot1=112.0
top1=200.0
range1=top1-bot1
while(at1 < et1):
    freq = random()*range1+bot1
    print("i1 %8.4f %8.4f %8.4f" % (at1, dur1, freq))
    at1 += step1
