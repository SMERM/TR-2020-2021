# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 07/12/2020

<!-- ## [Youtube video](https://youtu.be/kCq-hUzSqnQ) -->

### Tutorial sulle consegne

* su richiesta degli studenti viene fatto un primo tutorial sulle
  consegne delle lezioni precedenti.
  In particolare, viene [svolta in classe la prima consegna del 16 novembre
  2020](./tutorial-esercizio_1-20201116/README.md)
