# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 23/11/2020

## [Youtube video](https://youtu.be/kCq-hUzSqnQ)

### Verifica dello studio a casa

* csound?
* verifica 2 esercizi

#### Utilizzo di materiali concreti

* Elementi di rappresentazione spettrale dei suoni ![spectral decomposition](./spectral_decomposition.png)
* Materiali concreti
  * *objets sonores* (Schaeffer)
  * caratteristiche dei suoni e *music information retrieval* ![sound features](./sound_features.png)
    * *centroide*
    * densità spettrale
    * inviluppo (agogica)
  * [`freesound.org`](https://freesound.org)
  * `python` API per accedere a [`freesound.org`](https://freesound.org)
  * suoni strumentali utilizzati in forma concreta (*tape music*, Nono, Maderna *Musica su due dimensioni*)
* Editing e cross-fades convenzionali ![cross-fades](./cross-fades.png)
  * cross-fades lineari (segnali correlati/equal amplitude)
  * cross-fades esponenziali (segnali non-correlati/equal power)
* Editing e cross-fades non convenzionali
  * cross-fades "cinematografici" (*à la Bergman*)
  * *fono-mosaici*
  * cross-fades *spettrali* ![spectral cross fades](./spectral-cross-fades.png)
* Mixing concreto con sintetico
  * ripresa sintetica di caratteri morfologici (frequenze, agogica)
  * caricature sonore

### Utilizzo di `SuperCollider`

* collocazione di `SuperCollider` nel panorama strumentale
  * similitudini e differenze con `csound`
  * similitudini e differenze con `pure data`

### Studio a casa

* capitolo 4 del [FLOSS manual di `csound`](https://flossmanual.csound.com/sound-synthesis/additive-synthesis)
* installare `SuperCollider` e leggere i tutorials (facendo suonare gli esempi)
* due esercizi di csound:
  * esercizio 1: frammento di ca. 1 minuto con cross-fades *à la Bergman* di quattro suoni concreti (tratti da [`freesound.org`](https://freesound.org) oppure da una biblioteca personale)
  * esercizio 2: frammento di ca. 1 minuto con un migliaio di brevissimi frammenti raccolti e montati in forma algoritmica usando la API di [`freesound.org`](https://freesound.org) interpolati da sinusoidi
