# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 11/01/2021

[Youtube video](https://youtu.be/dQf5_WIieYo)

### Verifica dello studio a casa

* verifica consegne

### Rielaborazione metodologica del corso

* necessità di restituzione puntuale delle consegne
* elaborazione della consegna prossima ventura

### Consegna per il **16/01/2021 ore 12:00**

* raccogliere frammenti *concreti* in numero e qualità a piacere: dal
  [web](https://www.freesound.org), registrati autonomamente, *objets
  trouvés, ecc.
* *comporli* nel tempo in forma di *passacaglia* attraverso un editor di suoni
  (utilizzare un editor *semplice*, preferibilmente `audacity` o al massimo
  `ardour`)
* restrizioni: le uniche elaborazioni *permesse* sono *tagli* di tutte le
  fogge, *time-stretching* e *pitch-shifting*; non è concesso aggiungere altre
  elaborazioni *esterne* ai frammenti (ad es.  riverbero) 
* **termine perentorio di consegna: sabato 16 gennaio alle ore 12:00**

### SuperCollider

* Usi principali di `SuperCollider`:
  * *live-coding*
  * composizione algoritmica
  * installazioni
* Architettura di `SuperCollider`:
  * linguaggio: derivazione di `smalltalk`
  * *client-server*
    ![struttura SC](./struttura_sc.png)
  * `sclang`
  * libreria
  * server `superNova`
* Hands-on:
  * funzioni
  * suoni
  * interfacce di controllo

#### Esempi

* [Oscillatore con frequenza controllata da slider](./sc_slider.scd)
```SuperCollider
(
  var freqslider, synth;

  synth = {
    arg carfrq = 440;
    SinOsc.ar(carfrq, 0, 0.1);
  }.play; // if it doesn't "play" the slider won't work :-(

  freqslider = EZSlider(nil, 160@20, "frequenza", ControlSpec(20, 5000, 'exponential', 10, 440), {|ez|  synth.set(\carfrq, ez.value)});

)
```
