
LA = 220;
DO = 261.63;

nnote = 12;
powers = [0:nnote];
modulo = 2;

scala_di_DO = DO*(modulo.**(powers/nnote));
scala_di_LA = LA*(modulo.**(powers/nnote));

x = [1:nnote+4];


plot(x(1:nnote+1), scala_di_LA, '+', x(4:end), scala_di_DO, '+')
