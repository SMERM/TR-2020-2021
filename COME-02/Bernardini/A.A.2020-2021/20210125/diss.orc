sr=48000
nchnls=1
0dbfs=1


  instr 1
  iamp = ampdbfs(p6)
  ifreqstart=cpspch(p4)
  ifreqend  =cpspch(p5)

kfreq expon ifreqstart,p3,ifreqend
aout  oscil iamp, kfreq, 1
aout  linen aout, 0.01*p3,p3,0.01*p3

  out aout

  endin
