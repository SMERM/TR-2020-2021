
LA = 220.0;
DO = 261.6;

% in la:  la si  do# re  re#  mi  fa   sol sol#
% in do:  do re   mi fa  fa#  sol lab  sib  si
ratios = [1  9/8 5/4 4/3 11/8 3/2 13/8 7/4 15/8];

scala_di_DO = DO*ratios;
scala_di_LA = LA*ratios;

x = [1:length(ratios)+2];


plot(x(1:length(ratios)), scala_di_LA, '+', x(3:end), scala_di_DO, '+')
