# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 25/01/2021

## [Youtube video](https://youtu.be/Hk8IDI862rU)

### Verifica dello studio a casa

* verifica consegne
  * Putelli
  * Sanfeliz
  * De Mattei

### Consegna per la prossima volta

* Restrizioni:
  * mono
  * solo suoni sinusoidali
  * sistema non convenzionale di accordatura: niente ottave, niente 12 note,
    ma con tensione e distensione, risoluzioni
* Forma: canone a tre voci
  * canone rigoroso: tre voci a imitazione diretta (niente inversioni, voci
    libere - almeno nell'esposizione ecc.)
  * strumenti ammessi: aumentazioni, diminuzioni, pause di voce
  * sviluppo formale con soluzione
* Raccomandazioni:
  * massima libertà di scelta di:
    * spazi frequenziali
    * elaborazione ritmica
    * elaborazione agogica (durate, inviluppi, ecc.)

### Strategie di organizzazione parametrica: Gli spazi parametrici della composizione

#### Frequenza

* distribuzioni frequenziali
  * algoritmi distributivi
  * distribuzioni scalari
  * distribuzioni spettrali
  * distribuzioni psicoacusticamente informate
  * lo spettro come *real estate*
  * valenze estetiche
* sovrapposizioni frequenziali
  * banda critica
  * dissonanza e consonanza psicoacustiche

![funzionamenti delle scale](./funzionamenti_delle_scale.png)

#### Esempi in `octave`

* Problema delle intonazioni naturali:
  * scala di LA e scala di DO con intonazioni naturali
```matlab
LA = 220.0;
DO = 261.6;

% in la:  la si  do# re  re#  mi  fa   sol sol#
% in do:  do re   mi fa  fa#  sol lab  sib  si
ratios = [1  9/8 5/4 4/3 11/8 3/2 13/8 7/4 15/8];

scala_di_DO = DO*ratios;
scala_di_LA = LA*ratios;

x = [1:length(ratios)+2];


plot(x(1:length(ratios)), scala_di_LA, '+', x(3:end), scala_di_DO, '+')
```
  produce il grafico seguente:
  ![scale naturali](./scale_naturali.png)

* Soluzione temperata:
  * scala di LA e scala di DO con intonazione temperata
```matlab
LA = 220;
DO = 261.63;

nnote = 12;
powers = [0:nnote];
modulo = 2;

scala_di_DO = DO*(modulo.**(powers/nnote));
scala_di_LA = LA*(modulo.**(powers/nnote));

x = [1:nnote+4];


plot(x(1:nnote+1), scala_di_LA, '+', x(4:end), scala_di_DO, '+')
```
  produce il grafico seguente:
  ![scale temperate](./scale_temperate.png)

#### Ascolto della *banda critica* (*roughness*) in `csound`

* [Orchestra](./diss.orc)
```csound
sr=48000
nchnls=1
0dbfs=1


  instr 1
  iamp = ampdbfs(p6)
  ifreqstart=cpspch(p4)
  ifreqend  =cpspch(p5)

kfreq expon ifreqstart,p3,ifreqend
aout  oscil iamp, kfreq, 1
aout  linen aout, 0.01*p3,p3,0.01*p3

  out aout

  endin
```
* [Partitura](./diss.sco)
```csound
f1 0 16384 10 1 0.5 0.333 0.25 0.2 0.1666

i1 0 10 8.09 8.09 -12
i1 0 10 8.09 8.10 -12
```

Questa combinazione produce i suoni seguenti:

![diss.png](./diss.png)
