class Linear:

     def __init__(self, x0, y0, x1, y1):
         self.a = (y1 - y0) / (x1 - x0)
         self.b = -(self.a * x0) + y0
     def y(self,x):
         return x * self.a + self.b
