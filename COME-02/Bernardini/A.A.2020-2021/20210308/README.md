# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 08/03/2021

### [YouTube Video](https://youtu.be/sBliwtFtXpk)

### Verifica dello studio a casa

* verifica consegne: poliritmi (Polla)
  * ritmicizzare il frammento poliritmico della volta precedente

### Consegna per il 15/03/2021:

* Risolvere il problema software del brano (vedi qui di seguito)

### Realizzazione in classe del [compito](./pezzo.py)

Nella realizzazione in classe abbiamo adottato la suddivisione in moduli di
tutte le parti. Abbiamo quindi realizzato il compito in piccoli file, ciascuno
con specifiche funzioni. I file realizzati sono:

* Un file di [*contesto*](./contesto.py)
* La [funzione lineare](./linear.py) già utilizzata altrove
* Un oggetto [*voce*](./voce.py)
* Un oggetto [*pezzo*](./pezzo.py) che raccoglie tutti gli altri oggetti e li
  mette insieme (una sorta di *collante*)

#### [`Contesto`](./contesto.py)

La classe `Contesto` è un oggetto *celibe* (*singleton*): ne esiste una
istanza sola in tutto il programma, e questa istanza è globalmente visibile da
tutti (è la costante `C` inizializzata in fondo al file).

```python
from linear import Linear
class Contesto:
    def __init__(self, at, dur, ms, me):
        self.at = at
        self.dur = dur
        self.metro_start =ms
        self.metro_end=me
        self.tempofun=Linear(self.at,self.metro_start,self.at+self.dur,self.metro_end)


    def periodo(self,now):
        return 60 / self.tempofun.y(now)


C = Contesto(0, 60, 144,89)
```

#### [`Voce`](./voce.py)

```python
from contesto import C
class Voce:
    def __init__(self,note,ritmi):
        self.note=note
        self.ritmi=ritmi
    def to_csound(self):
        res=""
        start=C.at
        stop=C.at+C.dur
        now=start
        while now<stop:
            (line,step)=self.csound_lines(now)
            now+=step
            res+=line
        return res
    def csound_lines(self,t):
        res=""
        at = t
        for n in range (0,len(self.note)):
            if self.note[n]=="p":
                pass
            else:
                res+= ("i1 %8.4f %8.4f %8.4f \n" % (at, 0.15, self.note[n]))
            nstep=4*self.ritmi[n]*C.periodo(at)
            at+=nstep
        return [res,at]
```

#### [`Pezzo`](./pezzo.py)

```python
import pdb
from voce import Voce
v1=Voce([9.08,"p",9.08,9.08,9.08,"p"],[1/27,5/27,1/27,1/27,1/27,89/54],)
v2=Voce([8.09,"p",8.09,"p"],[1/20,3/20,1/20,1])
v3=Voce([8.08,8.08,"p",8.08,8.08,"p",8.08,8.08,"p",8.08,8.08],[1/28,1/28,3/28,1/28,1/28,1/2,1/28,1/28,3/28,1/28,1/28])
v4=Voce([8.04,8.04,"p",8.04,8.04],[1/10,1/10,1/10,1/10,1/10])
v5=Voce([7.07],[1])
v6=Voce([7.03,"p"],[1/4,1/4])
#pdb.set_trace()
print("f1 0 4096 10 1")
print(v1.to_csound())
print(v2.to_csound())
print(v3.to_csound())
print(v4.to_csound())
print(v5.to_csound())
print(v6.to_csound())
```

#### Risultato finale

Utilizzando [questa](./driver2.orc) di `csound`:
```csound
sr=48000
ksmps=10
nchnls=1
0dbfs=1

        instr 1
ifreq=cpspch(p4)
idur=p3
iatt=p2
iamp=1/6

kenv    expon  iamp, p3,    iamp/1000
aout    oscil  kenv, ifreq, 1

out aout
endin
```

Il risultato è ![questo](./pezzo.png)

Come si può constatare, c'è un errore nel programma che fa rallentare troppo
il metronomo (dovrebbe rallentare da 144 a 89)
