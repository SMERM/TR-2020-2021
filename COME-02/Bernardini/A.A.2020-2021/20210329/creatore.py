from vocenota import Voce
import yaml
import pdb

class Creatore:
    def __init__(self, file):
        self.file=file
        self.voci=[]
        self.setup()

    def setup(self):
       with open(self.file) as conf:
           self.dict=yaml.load(conf)
       #pdb.set_trace()
       for k,v in self.dict.items():
           if k != "metro":
               self.voci.append(Voce(v,self.dict["metro"]))

    def to_csound(self):
       for v in self.voci:
           v.to_csound()