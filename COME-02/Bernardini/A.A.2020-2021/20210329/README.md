# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 29/03/2021

## [Youtube video](https://youtu.be/tV484sbJ7rk)

### Verifica dello studio a casa

* Discussione degli elementi progettuali

## Argomento 1: gli array associativi in [`python`](./array_associativi.py)

```python
#
# array normale
#
a = [1, 2, 3, 4,]
try:
    print(a[0]) # -> 1
    print(a[3]) # -> 4
    print(a[100]) # -> None
except:
    print("errore")

#
# array associativo
#
a = { 'Milo': 'Pezzo concreto', 'Leonardo': 'Clair de lune', }
try:
    print(a['Milo']) # -> 'Pezzo concreto'
    print(a['Leonardo']) # -> 'Clair de lune' }
    print(a['nicb']) # -> None
except:
    print("errore")
```

## Argomento 2: i file di configurazione scritti in `yaml` (*Yet Another Markup Language*)

[conf0.yaml](./conf0.yaml)

```yaml
soprano:
        - { Freq: 220, Amp: pp, Ago: legato }
        - { Freq: 330, Amp: mf, Ago: staccato }
basso:
        - { Freq: 110, Amp: pp, Ago: legato }
        - { Freq: 116, Amp: ppp, Ago: staccato }
```

[conf1.yaml](./conf1.yaml)

```yaml
soprano:
        note: [ 8.09, 8.07, 8.00, 8.01 ]
        ritmi: [1/16.0, 1/16.0, 1/8.0, 1/4.0 ]
basso:
        note: [ 6.09, 6.07, 6.00, 6.01 ]
        ritmi: [1/16.0, 1/16.0, 1/8.0, 1/4.0 ]
```

## Argomento 3: utilizzo di file di configurazione `yaml` in `python`

[`Leonardo.yaml`](./Leonardo.yaml)
```yaml
metro: 65
melodia:
  note: [8.00, 8.01, 8.02, 8.03, 8.04]
  ritmi: [1/16.0, 1/16.0, 1/16.0, 1/16.0, 1/4.0 ]
  dinamiche: [p, pp, mf, f, pp,]
  agogica: [l, s, s, l, s]
accompagnamento:
  note: [[8.00, 8.01, 8.02], [8.03, 8.04],[8.00, 8.01, 8.02],[8.00, 8.01, 8.02],[8.00, 8.01, 8.02]]#accordi creati con sottoarray
  ritmi: [1/16.0, 1/16.0, 1/16.0, 1/16.0, 1/4.0 ]
  dinamiche: [p, pp, mf, f, pp,]
  agogica: [l, s, s, l, s]

```

[`nota.py`](./nota.py)
```python
class Nota:
    def __init__(self, at, dur, n, d):
        self.at=at
        self.dur=dur
        self.nota=n
        self.dinamica=d

    def to_csound(self):
        print("i1 %8.4f %8.4f %8.4f %8.4f" % (self.at, self.dur, self.nota, self.dinamica))

```

[`vocenota.py`](./vocenota.py)
```python
from nota import Nota
import pdb

class Voce:
    def __init__(self,v, m):
        self.voce_dict=v
        self.note=[]
        self.metro=m
        self.setup()

    def setup (self):
        self.periodo = 60/float(self.metro)
        dim = len(self.voce_dict["ritmi"])
        idx = 0
        at = 0
        while idx<dim:
            n = self.voce_dict["note"][idx]
            r = self.voce_dict["ritmi"][idx]
            d = self.voce_dict["dinamiche"][idx]
            a = self.voce_dict["agogica"][idx]
            if isinstance(n, float):
                self.note.append(self.create_note( at, n, r, d, a))
            if isinstance(n, list):
                for nn in n:
                    self.note.append(self.create_note(at, nn, r, d, a))
            idx += 1
            at += self.calc_dur(r)
    def create_note(self, at, n, r, d, a):
        return Nota(at, self.calc_dur(r)*self.convert_agogica(a), n, self.din_conv(d))


    def convert_agogica(self, a):
        a_dict={"l":1.1,"s":0.5}
        return a_dict[a]

    def calc_dur(self, dur):
        res=4*self.periodo*eval(dur)
        return res

    def din_conv(self, d):
        d_dict={"pp":-30, "p":-24, "mp": -18, "mf":-12, "f":-6, "ff":0}
        return d_dict[d]

    def to_csound(self):
        for n in self.note:
            n.to_csound()
```

[`creatore.py`](./creatore.py)
```python
from vocenota import Voce
import yaml
import pdb

class Creatore:
    def __init__(self, file):
        self.file=file
        self.voci=[]
        self.setup()

    def setup(self):
       with open(self.file) as conf:
           self.dict=yaml.load(conf)
       #pdb.set_trace()
       for k,v in self.dict.items():
           if k != "metro":
               self.voci.append(Voce(v,self.dict["metro"]))

    def to_csound(self):
       for v in self.voci:
           v.to_csound()
```

[`leonardomain.py`](./leonardomain.py)
```python
from creatore import Creatore
c=Creatore("Leonardo.yaml")
c.to_csound()
```

Eseguendo la seguente linea di codice:

```sh
$ python leonardomain.py
```

otterremo:


```sh
i1   0.0000   0.2538   8.0000 -24.0000
i1   0.2308   0.1154   8.0100 -30.0000
i1   0.4615   0.1154   8.0200 -12.0000
i1   0.6923   0.2538   8.0300  -6.0000
i1   0.9231   0.4615   8.0400 -30.0000
i1   0.0000   0.2538   8.0000 -24.0000
i1   0.0000   0.2538   8.0100 -24.0000
i1   0.0000   0.2538   8.0200 -24.0000
i1   0.2308   0.1154   8.0300 -30.0000
i1   0.2308   0.1154   8.0400 -30.0000
i1   0.4615   0.1154   8.0000 -12.0000
i1   0.4615   0.1154   8.0100 -12.0000
i1   0.4615   0.1154   8.0200 -12.0000
i1   0.6923   0.2538   8.0000  -6.0000
i1   0.6923   0.2538   8.0100  -6.0000
i1   0.6923   0.2538   8.0200  -6.0000
i1   0.9231   0.4615   8.0000 -30.0000
i1   0.9231   0.4615   8.0100 -30.0000
i1   0.9231   0.4615   8.0200 -30.0000
```

che è la partitura di csound corrispondente al frammento inserito nel file `yaml`.
