
#
# array normale
#
a = [1, 2, 3, 4,]
try:
    print(a[0]) # -> 1
    print(a[3]) # -> 4
    print(a[100]) # -> None
except:
    print("errore")

#
# array associativo
#
a = { 'Milo': 'Pezzo concreto', 'Leonardo': 'Clair de lune', }
try:
    print(a['Milo']) # -> 'Pezzo concreto'
    print(a['Leonardo']) # -> 'Clair de lune' }
    print(a['nicb']) # -> None
except:
    print("errore")
