from nota import Nota
import pdb

class Voce:
    def __init__(self,v, m):
        self.voce_dict=v
        self.note=[]
        self.metro=m
        self.setup()

    def setup (self):
        self.periodo = 60/float(self.metro)
        dim = len(self.voce_dict["ritmi"])
        idx = 0
        at = 0
        while idx<dim:
            n = self.voce_dict["note"][idx]
            r = self.voce_dict["ritmi"][idx]
            d = self.voce_dict["dinamiche"][idx]
            a = self.voce_dict["agogica"][idx]
            if isinstance(n, float):
                self.note.append(self.create_note( at, n, r, d, a))
            if isinstance(n, list):
                for nn in n:
                    self.note.append(self.create_note(at, nn, r, d, a))
            idx += 1
            at += self.calc_dur(r)
    def create_note(self, at, n, r, d, a):
        return Nota(at, self.calc_dur(r)*self.convert_agogica(a), n, self.din_conv(d))


    def convert_agogica(self, a):
        a_dict={"l":1.1,"s":0.5}
        return a_dict[a]

    def calc_dur(self, dur):
        res=4*self.periodo*eval(dur)
        return res

    def din_conv(self, d):
        d_dict={"pp":-30, "p":-24, "mp": -18, "mf":-12, "f":-6, "ff":0}
        return d_dict[d]

    def to_csound(self):
        for n in self.note:
            n.to_csound()
