sr=48000
ksmps=100
0dbfs=1
nchnls=1
zakinit 10,10


instr 1,2,3
ichan= p1-1
ifreqs mtof p4
ifreqe mtof p5
kfreq linseg ifreqs,p3*0.75,ifreqs,p3*0.25,ifreqe
zkw kfreq,ichan

endin


instr 101,102,103
ichan=p1-101
iamp= ampdbfs(p4)
kfreq zkr ichan
aout oscil iamp, kfreq, 1
aout linen aout, 0.01, p3, 0.05
out aout
endin

instr 11,12,13
ichan=p1-11
ivibfs=p6
ivibfe=p7
kvibf expon ivibfs, p3, ivibfe
if1 mtof p4
if2 mtof p5
ifrange= abs((if1-if2)/2)
ifoff min if1,if2
kfrange expon 0.01, p3, ifrange
kfreq oscil kfrange,kvibf,1
kfreq= kfreq+ifrange+ifoff
display kfreq, p3
zkw kfreq, ichan

endin