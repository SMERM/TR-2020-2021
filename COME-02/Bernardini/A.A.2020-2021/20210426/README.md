# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 26/04/2021

## [YouTube Video](https://youtu.be/sbZb8-15ndc)

## [Conversione tra MIDI note e pitch class](./mtop.py)

```python
def mtop(mn):
    oct = int(mn/12.0)
    semi = mn % 12.0
    return (oct + 3) + (semi/100.0)
```

## Elaborazione di strumenti combinati in `csound`) (*patching*)

![`csound` patching](./csound_patching.png)

### Esempio *hands-on*: [patcher.orc](./patcher.orc) e [patcher.sco](./patcher.sco)


```csound
sr=48000
ksmps=100
0dbfs=1
nchnls=1
zakinit 10,10


instr 1,2,3
ichan= p1-1
ifreqs mtof p4
ifreqe mtof p5
kfreq linseg ifreqs,p3*0.75,ifreqs,p3*0.25,ifreqe
zkw kfreq,ichan

endin


instr 101,102,103
ichan=p1-101
iamp= ampdbfs(p4)
kfreq zkr ichan
aout oscil iamp, kfreq, 1
aout linen aout, 0.01, p3, 0.05
out aout
endin

instr 11,12,13
ichan=p1-11
ivibfs=p6
ivibfe=p7
kvibf expon ivibfs, p3, ivibfe
if1 mtof p4
if2 mtof p5
ifrange= abs((if1-if2)/2)
ifoff min if1,if2
kfrange expon 0.01, p3, ifrange
kfreq oscil kfrange,kvibf,1
kfreq= kfreq+ifrange+ifoff
display kfreq, p3
zkw kfreq, ichan

endin
```

```csound
f1 0 4096 10 1

i101  0  6  -20
i1 0  1  72  70
i1 1  1  70  69
i1 2  1  69  67
i1 3  3  67  67
i102  0  6  -8
i12   0  6   67  77  0.5  6
i3 0  1  72  74
i3 1  1  74  75
i3 2  1  75  77
i3 3  3  77  77
i103  0  6  -20
```

Questa combinazione produce il suono che segue:

![patcher.wav](./patcher.wav.png)

e il plot della funzione modulante è:

![patcher modulation](./patcher-mod.png)
