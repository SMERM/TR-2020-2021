sr=48000
ksmps=10
nchnls=1
0dbfs=1

        instr 1
iamp    =   ampdbfs(p4)
ifreq   =   p5
idur    =   p3

aout    oscil  iamp, ifreq, 1
aout    linen  aout, 0.05*idur, idur, 0.05*idur

        out aout
        endin
