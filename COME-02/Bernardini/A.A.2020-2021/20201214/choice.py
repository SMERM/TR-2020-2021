from random import random

A = [3, 5, 7, 11, 13, 17, 19]

def choice(a):
    sz = len(a)
    idx = round(random()*(sz-1))
    return a[idx]


last = None

def choose_single(a):
    res = None
    while True:
        res = choice(a)
        if res != last:
            break
    last = res
    return res
