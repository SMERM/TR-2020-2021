from triangolo import TriangoloRettangolo

class Trishape(TriangoloRettangolo):

    def __init__(self, at, et, botf, topf, finalf):
        super().__init__(at, botf, topf, et, finalf)

    def to_csound(self, t, dur, amp):
        freq = self.choose_y(t)
        cs_string = "i1 %8.4f %8.4f %+5.2f %8.4f" % (t, dur, amp, freq) 
        print(cs_string)
