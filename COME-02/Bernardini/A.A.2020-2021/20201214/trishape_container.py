from linear import linear
from trishape import Trishape

class TrishapeContainer:

    def __init__(self, at, dur, f0, f1, f0range, n):
        self.at = at
        self.dur = dur
        self.f0 = f0
        self.f1 = f1
        self.f0range = f0range
        self.n = n
        self.triangoli = self.setup()

    def setup(self):
        res = []
        cur = 0
        tridur = self.dur / (self.n*2-1)
        step = tridur * 2
        initial_at = at = self.at
        while(cur < self.n):
           et = at + tridur
           f0center_start = linear(at, initial_at, self.f0, et, self.f1)
           f0center_end = linear(et, initial_at, self.f0, et, self.f1)
           topf = f0center_start + (0.5*self.f0range)
           botf = f0center_start - (0.5*self.f0range)
           res.append(Trishape(at, et, topf, botf, f0center_end))
           cur += 1
           at += step
        return res

    def to_csound(self, nnote):
        for t in self.triangoli:
            at = t.x0
            et = t.x2
            dur = et - at
            step = dur / (nnote - 1)
            while(at <= et):
                t.to_csound(at, dur, -30)
                at += step
