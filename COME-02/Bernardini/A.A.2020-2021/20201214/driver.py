from trishape import Trishape
from trishape_container import TrishapeContainer

bass = [
    Trishape(0, 7, 50, 200, 400),
    Trishape(14, 21, 50, 200, 400),
    Trishape(28, 35, 50, 200, 400),
    Trishape(42, 49, 50, 200, 400),
    Trishape(56, 63, 50, 200, 400),
]

soprano = TrishapeContainer(5, 55, 1500, 500, 300, 7)

print("f1 0 8192 10 1")

nnotes = 100

for t in bass:
    durrange = t.x2 - t.x0
    step = durrange/(nnotes-1)
    at = t.x0
    et = t.x2
    while(at <= et):
        t.to_csound(at, 0.1, -30)
        at += step

soprano.to_csound(nnotes)
