from random import random
from linear import linear

class Triangolo:

    def __init__(self, x0, y0, x1, y1, x2, y2):
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

    def choose_y(self, x):
        res = None
        if (x >= self.x0 and x <= self.x2):
            top = linear(x, self.x1, self.y1, self.x2, self.y2)
            bot = linear(x, self.x0, self.y0, self.x2, self.y2)
            rng = top-bot
            res = (random()*rng)+bot
        return res

class TriangoloRettangolo(Triangolo):

    def __init__(self, x0, y0, y1, x2, y2):
        super().__init__(x0, y0, x0, y1, x2, y2)
