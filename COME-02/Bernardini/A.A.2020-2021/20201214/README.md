# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 14/12/2020

<!-- ## [Youtube video](https://youtu.be/kCq-hUzSqnQ) -->

### Verifica delle consegne precedenti

### Tutorial sulle consegne

* su richiesta degli studenti viene fatto un secondo tutorial sulle
  consegne delle lezioni precedenti.
  In particolare, vengono:
  * svolta in classe la seconda consegna del 16 novembre 2020

#### Esercizio 2 consegna 16 novembre 2020

* [L'orchestra:](./driver.orc)
```csound
sr=48000
ksmps=10
nchnls=1
0dbfs=1

        instr 1
iamp    =   ampdbfs(p4)
ifreq   =   p5
idur    =   p3

aout    oscil  iamp, ifreq, 1
aout    linen  aout, 0.05*idur, idur, 0.05*idur

        out aout
        endin
```
* [driver python per la partitura](./driver.py)
```python
from trishape import Trishape
from trishape_container import TrishapeContainer

bass = [
    Trishape(0, 7, 50, 200, 400),
    Trishape(14, 21, 50, 200, 400),
    Trishape(28, 35, 50, 200, 400),
    Trishape(42, 49, 50, 200, 400),
    Trishape(56, 63, 50, 200, 400),
]

soprano = TrishapeContainer(5, 55, 1500, 500, 300, 7)

print("f1 0 8192 10 1")

nnotes = 100

for t in bass:
    durrange = t.x2 - t.x0
    step = durrange/(nnotes-1)
    at = t.x0
    et = t.x2
    while(at <= et):
        t.to_csound(at, 0.1, -30)
        at += step

soprano.to_csound(nnotes)
```
* considerazioni sul risultato
  * rendere meglio l'idea di *triangolo* (più densità all'inizio, meno alla fine)
  * versione *orientata* dei singoli frammenti
  * ![evoluzione](./evoluzione.png)
