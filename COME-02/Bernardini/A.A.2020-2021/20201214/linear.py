def linear(x, x0, y0, x1, y1):
    a = (y1-y0)/(x1-x0)
    b = -(a*x0)+y0
    return x*a+b
