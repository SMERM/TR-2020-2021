\version "2.18.2"

\header {
  title = "poliritmi a gogo"
  composer = "Leonardo, Sheila, Milo e Nicola"
  tagline = \markup {
    Engraved at
    \simple #(strftime "%Y-%m-%d" (localtime (current-time)))
    with \with-url #"http://lilypond.org/"
    \line { LilyPond \simple #(lilypond-version) (http://lilypond.org/) }
  }
}

snine = { \tuplet 9/8 { gis16 r4 r8 gis16 gis } \tuplet 9/8 { gis16 r2 } r2 r2 r2 }
sfive = { \tuplet 5/8 { a16 r8. a16 } r2 r2 }
sseven = {  \tuplet 7/8 { aes16 aes r8. aes16 aes} r2 \tuplet 7/8 { aes16 aes r8. aes16 aes} } 
sone = {  g2~ | g2 }
sfivetwo = { \tuplet 5/4 { e4 e r e e } }
sfour = { ees4 r }

\score {
  <<
  \new Staff {
    \clef treble
    \time 2/4
     \relative c''' { \snine \snine \snine \snine \snine  }
  }
  \new Staff {
    \clef treble
    \relative c'' { \sfive \sfive \sfive \sfive \sfive \sfive }
  }
  \new Staff {
    \clef treble
    \relative c'' { \sseven \sseven \sseven \sseven \sseven }
  }
  \new Staff {
    \clef treble
    \relative c' { \sfivetwo \sfivetwo \sfivetwo \sfivetwo \sfivetwo \sfivetwo \sfivetwo \sfivetwo \sfivetwo \sfivetwo }
  }
  \new Staff {
    \clef bass
    \relative c' {  \sone \sone \sone \sone \sone \sone \sone \sone \sone \sone  }
  }
  \new Staff {
    \clef bass
    \relative c { \sfour \sfour \sfour \sfour \sfour \sfour \sfour \sfour \sfour \sfour \sfour \sfour \sfour } 
  }
  >>
}
