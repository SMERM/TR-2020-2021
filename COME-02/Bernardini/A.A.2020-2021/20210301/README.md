# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 01/03/2021

### Verifica dello studio a casa

* verifica consegne: politritmi (De Mattei, Polla)
  * Realizzare un breve frammento di 7 voci a frequenze fisse con le seguenti
    disposizioni ritmiche:
    * pulsazione a mm = 20
    * suddivisione a 5
    * suddivisione a 6
    * suddivisione a 7
    * suddivisione a 8
    * suddivisione a 9
    * suddivisione a 10
    * suddivisione a 11

### Realizzazione in classe del [compito](./poliritmo.py)

```python
from random import random
import pdb
print ("f1 0 4096 10 1")
totdur = 30
at = 0
metro = 20
fullstep = 60/20
nvoci = 15
steps = [fullstep/s for s in range (1, nvoci+1)]
freq = [(100*n)+(random()*50)for n in range(1, nvoci+1)]
#pdb.set_trace()
while(at<totdur):
    idx = 0
    while (idx<nvoci):
        lat = at
        for n in range (0, idx+1):
          print ("i1 %8.4f %8.4f %8.4f; voce n.%d.%d" % (lat, steps[idx], freq[idx], idx+1, n))
          lat += steps[idx]
        idx += 1
    at += fullstep
```

### Realizzazione in classe di una variante multimetronomica del [compito](./poliritmo_multimetro.py)

```python
from random import random
import pdb
from linear import linear
print ("f1 0 4096 10 1")
totdur = 30
at = 0
nvoci = 15
steps = [1/s for s in range (1, nvoci+1)]
freq = [(100*n)+(random()*50)for n in range(1, nvoci+1)]
#pdb.set_trace()
while(at<totdur):
    metro = linear(at, 0, 20, totdur, 60)
    fullstep = 60/metro
    idx = 0
    while (idx<nvoci):
        lat = at
        for n in range (0, idx+1):
          step = steps [idx] * fullstep
          print ("i1 %8.4f %8.4f %8.4f; voce n.%d.%d" % (lat, step, freq[idx], idx+1, n))
          lat += step
        idx += 1
    at += fullstep
```

## Consegna per il 08/03/2021

* Basandosi sulle applicazioni fatte in classe costruire un frammento musicale
  che utilizzi la poliritmia in forma non triviale, differenziando gli aspetti
  ritmici e le pulsazioni. Un esempio di tale suddivisione - da utilizzare
  solo a titolo di esempio in notazione convenzionale - potrebbe essere
  [questo](./poliritmi-a-gogo.ly):
  ![poliritmi a gògò](./poliritmi-a-gogo.pdf)
  Ripetiamo: questo frammento è solo a titolo **esemplificativo**. La consegna
  consiste nel applicarsi a creare nuove formule ritmiche.
