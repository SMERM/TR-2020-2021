from random import random
import pdb
from linear import linear
print ("f1 0 4096 10 1")
totdur = 30
at = 0
nvoci = 15
steps = [1/s for s in range (1, nvoci+1)]
freq = [(100*n)+(random()*50)for n in range(1, nvoci+1)]
#pdb.set_trace()
while(at<totdur):
    metro = linear(at, 0, 20, totdur, 60)
    fullstep = 60/metro
    idx = 0
    while (idx<nvoci):
        lat = at
        for n in range (0, idx+1):
          step = steps [idx] * fullstep
          print ("i1 %8.4f %8.4f %8.4f; voce n.%d.%d" % (lat, step, freq[idx], idx+1, n))
          lat += step
        idx += 1
    at += fullstep