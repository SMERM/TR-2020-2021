TUNING=442
OCT=4
NOTE={"la":0,"sib":1,"si":2,"do":3,"do#":4,"re":5}#da finire
def ntof (nome,ottava):
    idx=NOTE[nome]
    freq=TUNING*(2.0**(idx/12.0))
    octfact = ottava-OCT
    octfact = 2.0**octfact
    return freq*octfact
