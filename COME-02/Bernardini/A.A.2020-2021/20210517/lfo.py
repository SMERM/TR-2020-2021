import math
class Lfo:
    def __init__(self,freq,amp,phase):
        self.freq = freq
        self.amp = amp
        self.phase = phase
    def y(self,t):
        return self.amp * math.cos(self.freq * math.pi*2*t + self.phase)