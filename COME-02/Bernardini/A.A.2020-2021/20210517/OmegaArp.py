from random import randint
from lfo import Lfo

class Arpeggio:
    def __init__(self,scala,ritmi,nnote,tempo):
        self.scala = scala
        self.ritmi = ritmi
        self.nnote = nnote
        self.tempo = tempo
        self.nota_idx=0
        self.ritmo_idx=0
        self.setup()

    def setup(self):
        self.arpeggio = []
        for n in range(0,self.nnote):
            idx = randint(0,len(self.scala)-1)
            self.arpeggio.append(self.scala[idx])

    def next(self,t):
        at = t
        nota = self.arpeggio[self.nota_idx]
        dur = self.ritmi[self.ritmo_idx]
        dur_t = 4.0*dur*(60.0/self.tempo)
        res = self.to_csound(at,dur_t,nota,)
        self.nota_idx += 1
        self.nota_idx = self.nota_idx % len(self.arpeggio)
        self.ritmo_idx += 1
        self.ritmo_idx = self.ritmo_idx % len(self.ritmi)
        return [res,dur_t]



    def to_csound(self,at,dur,freq):
        return ("i1 %8.4f %8.4f %8.4f \n" % (at,dur,freq))




    def run (self,da,a):
        now=da
        res=[]
        while (now<a):
            (line,dur)=self.next(now)
            now+=dur
            res.append(line)
        return "".join(res)

