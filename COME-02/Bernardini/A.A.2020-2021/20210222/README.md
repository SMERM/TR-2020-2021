# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 22/02/2021

## [Youtube video](https://youtu.be/aYYOpRUAdmc)

### Verifica dello studio a casa

* verifica consegne: progetto "canone a tre voci" (De Mattei)

### Ulteriore elaborazione di possibili suddivisioni frequenziali

* Elaborazione (a mo' di esempio) di un sistema di note "centrali"
  corredate da note "accessorie" (o di passaggio):
  ![scale con note di passaggio](./scale_con_note_di_passaggio.png)
* Elaborazione di un esempio in `octave`:
```matlab
clear -a
DO = 261.63;
base = 2;
nnote = 7;
f = DO*base.**([-14:14]/nnote);

np=[];
for n=2:length(f)
  diff=f(n)-f(n-1);
  div=2;
  if diff>25
    div=3;
  endif
  fratio=f(n)/f(n-1);
  for k=1:div-1
    nf= f(n-1)*(fratio**(k/div));
    np=horzcat(np,nf);
  end
end
%hold on
%plot([1:length(f)],f,'+')
%plot([1:length(np)],np,'*')
%hold off
total=horzcat(f,np);
total=sort(total);
plot([1:length(total)],total,'+')
```

Questo script produce il grafico seguente:

![scale e note di passaggio](./scale_e_note_di_passaggio.png)

### Organizzazione temporale degli eventi

* pulsazione
* tempo
  * conversione metronomo -> tempo lineare
* suddivisione metrica
  * metri pari
  * metri dispari
* metro
  * suddivisione degli accenti (rif. alla poesia)
    * *trocheo* (-U) (battere-levare)
    * *spondeo* (--) (battere-battere)
    * *iambo* (o *giambo*) (U-) (levare-battere)
    * *dattilo* (-UU) (3/4)
    * *anfibrachio* (U-U) (3/4)
* suddivisione ritmica
* *consonanze* e *dissonanze* ritmiche

### Consegna per il 01/03/2021

* Realizzare un breve frammento di 7 voci a frequenze fisse con le seguenti
  disposizioni ritmiche:
  * pulsazione a mm = 20
  * suddivisione a 5
  * suddivisione a 6
  * suddivisione a 7
  * suddivisione a 8
  * suddivisione a 9
  * suddivisione a 10
  * suddivisione a 11
