clear -a
DO = 261.63;
base = 2;
nnote = 7;
f = DO*base.**([-14:14]/nnote);



np=[];
for n=2:length(f)
  diff=f(n)-f(n-1);
  div=2;
  if diff>25
    div=3;
  endif
  fratio=f(n)/f(n-1);
  for k=1:div-1
    nf= f(n-1)*(fratio**(k/div));
    np=horzcat(np,nf);
  end
end
%hold on
%plot([1:length(f)],f,'+')
%plot([1:length(np)],np,'*')
%hold off
total=horzcat(f,np);
total=sort(total);
plot([1:length(total)],total,'+')