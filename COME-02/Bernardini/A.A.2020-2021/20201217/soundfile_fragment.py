from soundfile import Soundfile

class SoundfileFragment:

    def __init__(self, s, t, skip, dur):
        self.soundfile = s
        self.at = t
        self.skip = skip
        self.dur = dur

    def to_csound(self):
        return("i1 %8.4f %8.4f %+5.2f %d %8.4f" % (self.at, self.dur, -3, self.soundfile.numero, self.skip))
