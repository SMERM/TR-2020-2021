from soundfile import Soundfile
from crossfader import CrossFader

s1 = Soundfile(1, 12.181)
s2 = Soundfile(2, 4.056)

cf = CrossFader(s1, s2, 0, 0.6)

cf.to_csound()
