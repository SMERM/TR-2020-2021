# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 17/12/2020

## [Youtube video](https://youtu.be/ldPUEVQI6ro)

### Verifica delle consegne precedenti

### Tutorial sulle consegne

* su richiesta degli studenti viene fatto un secondo tutorial sulle
  consegne delle lezioni precedenti.
  In particolare, vengono:
  * svolta in classe la prima consegna del 30 novembre 2020

### Esercizio 1 30 novembre 2020

* [orchestra `csound`](./driver.orc)
```csound
sr=44100
ksmps=10
nchnls=1
0dbfs=1


        instr 1
iamp    = ampdbfs(p4)
ifile   = p5
iskip   = p6
idur    = p3

aout    soundin ifile, iskip
aout    linen   aout, 0.001*idur,idur,0.001*idur

        out aout
        endin
```
* esposizione del problema:
  ![incrocio](./incrocio.png)
* [driver `python`](./driver.py)
```python
from soundfile import Soundfile
from crossfader import CrossFader

s1 = Soundfile(1, 12.181)
s2 = Soundfile(2, 4.056)

cf = CrossFader(s1, s2, 0, 0.6)

cf.to_csound()
```
  **NOTA BENE**: il software così come realizzato il 17/12/2020 non funziona (ancora) bene
