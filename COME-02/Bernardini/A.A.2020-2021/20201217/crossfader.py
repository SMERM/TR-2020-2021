from soundfile import Soundfile
from soundfile_fragment import SoundfileFragment
from linear import linear

class CrossFader:

    def __init__(self, s1, s2, at, pdt):
        self.soundfile_1 = s1
        self.soundfile_2 = s2
        self.at = at
        self.durata_totale = (self.soundfile_1.durata+self.soundfile_2.durata) * pdt
        self.combinazione = []
        self.crossfade()

    def crossfade(self):
        first_frag = self.soundfile_1.durata * 0.1
        last_frag = self.soundfile_2.durata * 0.1
        t = self.at
        et = self.at + self.durata_totale
        while (t < et):
            durfrag1 = linear(t, 0, first_frag, self.soundfile_1.durata, 0)
            durfrag2 = linear(t+durfrag1, 0, 0, self.soundfile_2.durata, last_frag)
            frag1 = SoundfileFragment(self.soundfile_1, t, t-self.at, durfrag1)
            frag2 = SoundfileFragment(self.soundfile_2, t+durfrag1, t+durfrag1-self.at, durfrag2)
            self.combinazione.append(frag1)
            self.combinazione.append(frag2)
            t += (durfrag1+durfrag2)

    def to_csound(self):
        for l in self.combinazione:
            print(l.to_csound())
