sr=44100
ksmps=10
nchnls=1
0dbfs=1


        instr 1
iamp    = ampdbfs(p4)
ifile   = p5
iskip   = p6
idur    = p3

aout    soundin ifile, iskip
aout    linen   aout, 0.001*idur,idur,0.001*idur

        out aout
        endin
