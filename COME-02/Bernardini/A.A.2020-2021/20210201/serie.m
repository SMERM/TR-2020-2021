%
% illustrazione dei principi seriali di base
%
% L'illustrazione viene fatta con la serie usata da Arnold Schoenberg
% nel brano per pianoforte op.33a
%
op33a_orig = [ 10 5 0 11 9 6 1 3 7 8 2 4 ];

function r = transpose(o0)
  r = zeros(12,12);
  for k=1:12
    r(k,:) = mod(o0 + (k-1),12);
  end
  r;
end

function r = invert(o0)
  r = zeros(12,12);
  last = r(1,1) = o0(1,1);
  for k=2:12
    intv = o0(1,k) - o0(1,k-1);
    next = mod(last - intv, 12);
    r(1,k) = next;
    last = next;
  end
  r = transpose(r(1,:));
end

function r = retrograde(o0)
  rorig = fliplr(o0); % invert
  r = transpose(rorig);
end

function r = retrograde_invert(o0)
  riorig = fliplr(o0);
  r = invert(riorig);
end

op33a_O = transpose(op33a_orig);
op33a_I = invert(op33a_orig);
op33a_R = retrograde(op33a_orig);
op33a_RI= retrograde_invert(op33a_orig);


for k=1:length(op33a_O)
  printf("O%02d:\t\t%2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d\n", k-1, op33a_O(k,:));
end

printf("\n\n");

for k=1:length(op33a_I)
  printf("I%02d:\t\t%2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d\n", k-1, op33a_I(k,:));
end

printf("\n\n");

for k=1:length(op33a_R)
  printf("R%02d:\t\t%2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d\n", k-1, op33a_R(k,:));
end

printf("\n\n");

for k=1:length(op33a_RI)
  printf("RI%02d:\t\t%2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d\n", k-1, op33a_RI(k,:));
end
