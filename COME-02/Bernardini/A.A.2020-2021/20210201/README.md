# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 01/02/2021

## [Youtube video](https://youtu.be/HPuNbEKujTY)

### Verifica dello studio a casa

* verifica consegne: progetto "canone a tre voci"

### Strategie di organizzazione parametriche

#### Organizzazioni casuali

* musicalità della casualità
  * ridondanza e variazione

#### Organizzazioni combinatorie

* combinazioni intervallari (*The Structure of Atonal Music*, Allen Forte)
  * classi di altezze
  * vettori intervallari
  * forma primaria
    * [database di forme primarie](./sets.md)
  * trasformazioni
    * trasposizioni
    * inversioni
    * retrogradazioni
* estensioni ad altri parametri
  * ritmo

#### Organizzazioni seriali

* serie
  * principi di serialità
    * regole di serializzazione
  * trasformazioni
    * trasposizioni
    * inversioni
    * retrogradi
    * inversioni retrograde
  * rotazioni
  * permutazioni
* applicazioni musicali delle serie
  * dodecafonia
  * serialismo integrale
  * altre serie frequenziali (spettralismo, ...)
* applicazioni alla musica elettroacustica
  * serie parametriche
  * rilevanza della lunghezza della serie
* estensioni al ritmo:
  * ritmi retrogradi
  * funzioni ritmiche
  ![ritmi sintetici](./ritmi_sintetici.png)

#### [Realizzazione delle trasformazioni seriali classiche realizzate in `octave`](./serie.m)

```matlab
%
% illustrazione dei principi seriali di base
%
% L'illustrazione viene fatta con la serie usata da Arnold Schoenberg
% nel brano per pianoforte op.33a
%
op33a_orig = [ 10 5 0 11 9 6 1 3 7 8 2 4 ];

function r = transpose(o0)
  r = zeros(12,12);
  for k=1:12
    r(k,:) = mod(o0 + (k-1),12);
  end
  r;
end

function r = invert(o0)
  r = zeros(12,12);
  last = r(1,1) = o0(1,1);
  for k=2:12
    intv = o0(1,k) - o0(1,k-1);
    next = mod(last - intv, 12);
    r(1,k) = next;
    last = next;
  end
  r = transpose(r(1,:));
end

function r = retrograde(o0)
  rorig = fliplr(o0); % invert
  r = transpose(rorig);
end

function r = retrograde_invert(o0)
  riorig = fliplr(o0);
  r = invert(riorig);
end

op33a_O = transpose(op33a_orig);
op33a_I = invert(op33a_orig);
op33a_R = retrograde(op33a_orig);
op33a_RI= retrograde_invert(op33a_orig);


for k=1:length(op33a_O)
  printf("O%02d:\t\t%2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d\n", k-1, op33a_O(k,:));
end

printf("\n\n");

for k=1:length(op33a_I)
  printf("I%02d:\t\t%2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d\n", k-1, op33a_I(k,:));
end

printf("\n\n");

for k=1:length(op33a_R)
  printf("R%02d:\t\t%2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d\n", k-1, op33a_R(k,:));
end

printf("\n\n");

for k=1:length(op33a_RI)
  printf("RI%02d:\t\t%2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d %2d\n", k-1, op33a_RI(k,:));
end
```
  Questo script produce:
```
O00:		10  5  0 11  9  6  1  3  7  8  2  4
O01:		11  6  1  0 10  7  2  4  8  9  3  5
O02:		 0  7  2  1 11  8  3  5  9 10  4  6
O03:		 1  8  3  2  0  9  4  6 10 11  5  7
O04:		 2  9  4  3  1 10  5  7 11  0  6  8
O05:		 3 10  5  4  2 11  6  8  0  1  7  9
O06:		 4 11  6  5  3  0  7  9  1  2  8 10
O07:		 5  0  7  6  4  1  8 10  2  3  9 11
O08:		 6  1  8  7  5  2  9 11  3  4 10  0
O09:		 7  2  9  8  6  3 10  0  4  5 11  1
O10:		 8  3 10  9  7  4 11  1  5  6  0  2
O11:		 9  4 11 10  8  5  0  2  6  7  1  3


I00:		10  3  8  9 11  2  7  5  1  0  6  4
I01:		11  4  9 10  0  3  8  6  2  1  7  5
I02:		 0  5 10 11  1  4  9  7  3  2  8  6
I03:		 1  6 11  0  2  5 10  8  4  3  9  7
I04:		 2  7  0  1  3  6 11  9  5  4 10  8
I05:		 3  8  1  2  4  7  0 10  6  5 11  9
I06:		 4  9  2  3  5  8  1 11  7  6  0 10
I07:		 5 10  3  4  6  9  2  0  8  7  1 11
I08:		 6 11  4  5  7 10  3  1  9  8  2  0
I09:		 7  0  5  6  8 11  4  2 10  9  3  1
I10:		 8  1  6  7  9  0  5  3 11 10  4  2
I11:		 9  2  7  8 10  1  6  4  0 11  5  3


R00:		 4  2  8  7  3  1  6  9 11  0  5 10
R01:		 5  3  9  8  4  2  7 10  0  1  6 11
R02:		 6  4 10  9  5  3  8 11  1  2  7  0
R03:		 7  5 11 10  6  4  9  0  2  3  8  1
R04:		 8  6  0 11  7  5 10  1  3  4  9  2
R05:		 9  7  1  0  8  6 11  2  4  5 10  3
R06:		10  8  2  1  9  7  0  3  5  6 11  4
R07:		11  9  3  2 10  8  1  4  6  7  0  5
R08:		 0 10  4  3 11  9  2  5  7  8  1  6
R09:		 1 11  5  4  0 10  3  6  8  9  2  7
R10:		 2  0  6  5  1 11  4  7  9 10  3  8
R11:		 3  1  7  6  2  0  5  8 10 11  4  9


RI00:		 4  6  0  1  5  7  2 11  9  8  3 10
RI01:		 5  7  1  2  6  8  3  0 10  9  4 11
RI02:		 6  8  2  3  7  9  4  1 11 10  5  0
RI03:		 7  9  3  4  8 10  5  2  0 11  6  1
RI04:		 8 10  4  5  9 11  6  3  1  0  7  2
RI05:		 9 11  5  6 10  0  7  4  2  1  8  3
RI06:		10  0  6  7 11  1  8  5  3  2  9  4
RI07:		11  1  7  8  0  2  9  6  4  3 10  5
RI08:		 0  2  8  9  1  3 10  7  5  4 11  6
RI09:		 1  3  9 10  2  4 11  8  6  5  0  7
RI10:		 2  4 10 11  3  5  0  9  7  6  1  8
RI11:		 3  5 11  0  4  6  1 10  8  7  2  9
```

### SuperCollider

* Architettura di `SuperCollider`:
  * libreria
* Hands-on:
  * Inviluppi di ampiezza
