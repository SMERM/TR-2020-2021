%
% riferimento: scala naturale
%
intervalli_naturali = [ 1 9/8 5/4 4/3 11/8 3/2 5/3 7/4 15/8 2 ];
freq0 = 261.6;

scala_naturale = [ (freq0 * intervalli_naturali) (freq0*2 * intervalli_naturali) ]

%
% costruiamo un sistema temperato con module 3 e 13 note 
%
modulo = 3;
nnote = 13;
scala_sintetica = [ freq0 * (modulo.**([0:nnote]/nnote)) freq0*modulo * (modulo.**([0:nnote]/nnote)) ]
max_indx = min(length(scala_naturale), length(scala_sintetica));

distanze = abs(scala_naturale(1:max_indx) .- scala_sintetica(1:max_indx))
