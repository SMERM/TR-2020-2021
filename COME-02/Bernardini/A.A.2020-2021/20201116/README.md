# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 16/11/2020

### Verifica dello studio a casa

* allenamento dell'orecchio?
* installazione software?
* csound?
* verifica 4 esercizi

### Argomenti

* progetti annuali
  * I° anno: composizione acusmatica
  * II° anno: composizione strumento e fixed media
  * III° anno: composizione strumento e live-electronics
* spazi segmentati vs. spazi continui
  * grammatiche vs. funzioni matematiche
  * casualità e probabilità

#### Strategie di organizzazione parametrica in spazi continui

* principi di casualità
  * rumore bianco (equal amp), rumore rosa (equal power) -> probabilità
  * casualità controllata: offset e range
    * offset e range statici
    * offset e range dinamici
* `python` software per scrivere partiture `csound`
* applicazione alle altezze

##### Principi di casualità

###### rumore bianco (equal amp), rumore rosa (equal power)

![white_vs_pink](./noise.png)

Il rumore rosa si realizza prendendo il rumore bianco e facendo la somma del
campione attuale col campione precedente:

![noise](./white_vs_pink.png)

###### [casualità controllata 1](./primo.py):

Sola applicazione alle frequenze

```python
from random import random

nnotes = 100

step = 0.125
dur  = step/2
amp = 10000
top = 8000
bot = 100
frange = top - bot

n = 0

print("f1 0 8192 10 1")

at = 0
last_freq = bot

while (n < nnotes):
    freq = random()*frange+bot
    freq = (freq + last_freq) / 2
    print("i1 %8.4f %8.4f %8.4f %8.4f" % (at, dur, amp, freq))
    at = at + step
    n = n + 1
    freq = last_freq
```

Le frequenze sono casuali ma scelte tra 100 e 8000 Hz. La funzione `random()`
produce un numero casuale uniformemente distribuito tra 0 e 1. Questo numero
viene poi moltiplicato per la variabile `frange` (`8000-100`) e sommato alla
variabile `bot` (`100`).

###### [casualità controllata 2](./secondo.py):

Applicazione alle frequenze, alle durate, agli *action-time* e alle ampiezze

```python
from random import random

nnotes = 1000

totdur = 30
top = 8000
bot = 100
frange = top - bot
durmax = 3
durmin = 0.2
durrange = durmax - durmin
ampmax = 1500
ampmin = 150
amprange = ampmax - ampmin

n = 0

print("f1 0 8192 10 1")

last_freq = bot

while (n < nnotes):
    at = random()*totdur
    freq = random()*frange+bot
    freq = (freq + last_freq) / 2
    dur = random()*durrange+durmin
    amp = random()*amprange+ampmin
    print("i1 %08.4f %8.4f %8.4f %8.4f" % (at, dur, amp, freq))
    n = n + 1
    freq = last_freq
```

[secondo bis](./secondo-bis.py)

```python
from random import random

nnotes = 1000

totdur = 30
top = 8000
bot = 100
frange = top - bot
durmax = 0.1
durmin = 0.01
durrange = durmax - durmin
ampmax = 1500
ampmin = 150
amprange = ampmax - ampmin

n = 0

print("f1 0 8192 10 1")

last_freq = bot

while (n < nnotes):
    at = random()*totdur
    freq = random()*frange+bot
    freq = (freq + last_freq) / 2
    dur = random()*durrange+durmin
    amp = random()*amprange+ampmin
    print("i1 %08.4f %8.4f %8.4f %8.4f" % (at, dur, amp, freq))
    n = n + 1
    freq = last_freq
```

###### [terzo.py](./terzo.py)

Aggiunta di funzioni lineari per rendere il range dinamico

![dynamic range](./dynamic_range.png)

```python
from random import random

def linear(y0, y1, x0, x1, x):
    a = (y1 - y0) / (x1 - x0)
    b = y0 - a*x0
    return a*x + b

nnotes = 1000

totdur = 15
top0 = 5000
bot0 = 4000
top1 = 80
bot1 = 80
durmax = 0.3
durmin = 0.03
durrange = durmax - durmin
ampmax = 1500
ampmin = 150
amprange = ampmax - ampmin

n = 0

print("f1 0 8192 10 1")

while (n < nnotes):
    at = random()*totdur
    top = linear(top0, top1, 0, totdur, at)
    bot = linear(bot0, bot1, 0, totdur, at)
    frange = top - bot
    freq = random()*frange+bot
    dur = random()*durrange+durmin
    amp = random()*amprange+ampmin
    print("i1 %08.4f %8.4f %8.4f %8.4f" % (at, dur, amp, freq))
    n = n + 1
```

#### [driver.orc](./driver.orc)

Tutte le prove fatte in classe si possono realizzare con la seguente
*orchestra*:

```csound
sr=44100
ksmps=100
nchnls=1


    instr 1
iamp = p4
ifreq = p5 

aout oscil iamp, ifreq, 1
aout linen aout, p3*0.01, p3, p3*0.01

     out   aout

    endin
```

### Studio a casa

* due esercizi di csound:
  * esercizio 1: frammento di ca. 1 minuto con 4 fasce sonore continue e ben differenziate (differenziazione dinamica)
  * esercizio 2: frammento di ca. 1 minuto con 4 fasce sonore strutturate ritmicamente e ben differenziate (differenziazione ritmica)

![esercizi](./esercizi.png)

(le figure sono solo a titolo esemplificativo)
