from random import random

nnotes = 1000

totdur = 30
top = 8000
bot = 100
frange = top - bot
durmax = 3
durmin = 0.2
durrange = durmax - durmin
ampmax = 1500
ampmin = 150
amprange = ampmax - ampmin

n = 0

print("f1 0 8192 10 1")

last_freq = bot

while (n < nnotes):
    at = random()*totdur
    freq = random()*frange+bot 
    freq = (freq + last_freq) / 2
    dur = random()*durrange+durmin
    amp = random()*amprange+ampmin
    print("i1 %08.4f %8.4f %8.4f %8.4f" % (at, dur, amp, freq))
    n = n + 1
    freq = last_freq
