sr=44100
ksmps=100
nchnls=1


    instr 1
iamp = p4
ifreq = p5 

aout oscil iamp, ifreq, 1
aout linen aout, p3*0.01, p3, p3*0.01

     out   aout

    endin
