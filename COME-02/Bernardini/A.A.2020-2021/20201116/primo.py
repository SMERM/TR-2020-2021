from random import random

nnotes = 100

step = 0.125
dur  = step/2
amp = 10000
top = 8000
bot = 100
frange = top - bot

n = 0

print("f1 0 8192 10 1")

at = 0
last_freq = bot

while (n < nnotes):
    freq = random()*frange+bot 
    freq = (freq + last_freq) / 2
    print("i1 %8.4f %8.4f %8.4f %8.4f" % (at, dur, amp, freq))
    at = at + step
    n = n + 1
    freq = last_freq
