from random import random

def linear(y0, y1, x0, x1, x):
    a = (y1 - y0) / (x1 - x0)
    b = y0 - a*x0
    return a*x + b

nnotes = 1000

totdur = 15
top0 = 5000
bot0 = 4000
top1 = 80
bot1 = 80
durmax = 0.3
durmin = 0.03
durrange = durmax - durmin
ampmax = 1500
ampmin = 150
amprange = ampmax - ampmin

n = 0

print("f1 0 8192 10 1")

while (n < nnotes):
    at = random()*totdur
    top = linear(top0, top1, 0, totdur, at)
    bot = linear(bot0, bot1, 0, totdur, at)
    frange = top - bot
    freq = random()*frange+bot 
    dur = random()*durrange+durmin
    amp = random()*amprange+ampmin
    print("i1 %08.4f %8.4f %8.4f %8.4f" % (at, dur, amp, freq))
    n = n + 1
