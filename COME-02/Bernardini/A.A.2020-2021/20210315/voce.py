from contesto import C
class Voce:
    def __init__(self,note,ritmi):
        self.note=note
        self.ritmi=ritmi
    def to_csound(self):
        res=""
        start=C.at
        stop=C.at+C.dur
        now=start
        while now<stop:
            (line,next_at)=self.csound_lines(now)
            now=next_at
            res+=line
        return res
    def csound_lines(self,t):
        res=""
        at = t
        for n in range (0,len(self.note)):
            if self.note[n]=="p":
                pass
            else:
                res+= ("i1 %8.4f %8.4f %8.4f \n" % (at, 0.15, self.note[n]))
            nstep=4*self.ritmi[n]*C.periodo(at)
            at+=nstep
        return [res,at]
