sr=48000
ksmps=2
nchnls=1
0dbfs=1


      instr 1
iamp  = ampdbfs(p4)
ifreq = cpspch(p5)
idur  = p3
ioff  = 0.01

kenv  expon iamp+ioff, idur, ioff
kenv  = kenv - ioff

aout  oscil  kenv, ifreq, 1

      out aout
      endin

      instr 2
iamp  = ampdbfs(p4)
ifstart = cpspch(p5)
ifstop  = cpspch(p6)
idur  = p3
ioff  = 0.01

kenv  expon iamp+ioff, idur, ioff
kenv  = kenv - ioff

kfreq expon ifstart, idur, ifstop

aout  oscil  kenv, kfreq, 1

      out aout
      endin

      instr 3
iamp  = ampdbfs(p4)
ifstart = cpspch(p5)
ifstop  = cpspch(p6)
iavib   = p7
idur  = p3
ioff  = 0.01

kenv  expon iamp+ioff, idur, ioff
kenv  = kenv - ioff

kvibenv linseg 0,idur*0.05,0,idur*.95,iavib
kvib  oscil kvibenv, 4, 1
kfreq expon ifstart, idur, ifstop
kvib  = (kvib/2) + (kfreq/2)
kfreq = kfreq + kvib

display kfreq, idur

aout  oscil  kenv, kfreq, 1

      out aout
      endin

      instr 4
iamp  = ampdbfs(p4)
ifstart = cpspch(p5)
ifstop  = cpspch(p6)
iavib   = p7
idur  = p3
ioff  = 0.01
ivibs = 4
ivibe = 12

kenv  expon iamp+ioff, idur, ioff
kenv  = kenv - ioff

kvibenv linseg 0,idur*0.05,0,idur*.95,iavib
kvibf  expon ivibs,idur,ivibe
kvib  oscil kvibenv, kvibf, 1
kfreq expon ifstart, idur, ifstop
kvib  = (kvib/2) + (kfreq/2)
kfreq = kfreq + kvib

display kfreq, idur

aout  oscil  kenv, kfreq, 1

      out aout
      endin

      instr 5
iamp  = ampdbfs(p4)
ifstart = cpspch(p5)
ifstop  = cpspch(p6)
iavib   = p7
idur  = p3
ioff  = 0.01
ivibs = 2
ivibe = 5
idiff = 0.3418059

kenv  expon iamp+ioff, idur, ioff
kenv  = kenv - ioff

kvibenv linseg 0,idur*0.05,0,idur*.95,iavib
kvibf  expon ivibs,idur,ivibe
kvib  oscil kvibenv, kvibf, 1
kfreq expon ifstart, idur, ifstop
kvib  = (kvib/2) + (kfreq/2)
kfreq = kfreq + kvib

display kfreq, idur

a1    oscil  kenv, kfreq, 2
a2    oscil  kenv, kfreq+(idiff), 2
a3    oscil  kenv, kfreq+(2*idiff), 2
a4    oscil  kenv, kfreq+(3*idiff), 2
a5    oscil  kenv, kfreq+(4*idiff), 2
a6    oscil  kenv, kfreq+(5*idiff), 2

      out (a1+a2+a3+a4+a5+a6)/6
      endin
