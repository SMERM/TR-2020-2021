# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 15/03/2021

<!-- ## [Youtube video](https://youtu.be/HPuNbEKujTY) -->

### Verifica dello studio a casa

* verifica consegne: correzione dell'algoritmo proposto nella lezione
  precedente
* discussione degli elaborati per l'esame di Composizione Musicale
  Elettroacustica I:
  * brano acusmatico
  * materiali: suoni sintetici semplici e/o suoni concreti
  * durata: 3'-5'

### Consegna per il 22/03/2021

* Portare il primo abbozzo di progetto per brano d'esame

### Correzione dell'algoritmo proposto nella [lezione precedente](../20210308/README.md)

* [`pezzo.py`](./pezzo.py)
```python
import pdb
from voce import Voce
v1=Voce([9.08,"p",9.08,9.08,9.08,"p"],[1/18,5/18,1/18,1/18,1/18,22/9],)
v2=Voce([8.09,"p",8.09,"p"],[1/10,3/10,1/10,1])
v3=Voce([8.08,8.08,"p",8.08,8.08,"p",8.08,8.08,"p",8.08,8.08],[1/14,1/14,3/14,1/14,1/14,1/2,1/14,1/14,3/14,1/14,1/14])
v4=Voce([8.04,8.04,"p",8.04,8.04],[1/5,1/5,1/5,1/5,1/5])
v5=Voce([7.07],[1])
v6=Voce([7.03,"p"],[1/4,1/4])
#pdb.set_trace()
print("f1 0 4096 10 1")
print(v1.to_csound())
print(v2.to_csound())
print(v3.to_csound())
print(v4.to_csound())
print(v5.to_csound())
print(v6.to_csound())
```
* [`voce.py`](./voce.py)
```python
from contesto import C
class Voce:
    def __init__(self,note,ritmi):
        self.note=note
        self.ritmi=ritmi
    def to_csound(self):
        res=""
        start=C.at
        stop=C.at+C.dur
        now=start
        while now<stop:
            (line,next_at)=self.csound_lines(now)
            now=next_at
            res+=line
        return res
    def csound_lines(self,t):
        res=""
        at = t
        for n in range (0,len(self.note)):
            if self.note[n]=="p":
                pass
            else:
                res+= ("i1 %8.4f %8.4f %8.4f \n" % (at, 0.15, self.note[n]))
            nstep=4*self.ritmi[n]*C.periodo(at)
            at+=nstep
        return [res,at]
```
(è stata corretta la terzultima riga del metodo `to_csound()`)
* (i file [`contesto.py`](./contesto.py), [`linear.py`](./linear.py) e [`driver2.orc`](./driver2.orc)
  sono rimasti invariati)

Lo spettrogramma del brano corretto è il seguente:

![pezzo](./pezzo.png)

### Esempi di "movimentazione" di suoni sinusoidali

* [orchestra](./driver.orc)
```csound
sr=48000
ksmps=2
nchnls=1
0dbfs=1


      instr 1
iamp  = ampdbfs(p4)
ifreq = cpspch(p5)
idur  = p3
ioff  = 0.01

kenv  expon iamp+ioff, idur, ioff
kenv  = kenv - ioff

aout  oscil  kenv, ifreq, 1

      out aout
      endin

      instr 2
iamp  = ampdbfs(p4)
ifstart = cpspch(p5)
ifstop  = cpspch(p6)
idur  = p3
ioff  = 0.01

kenv  expon iamp+ioff, idur, ioff
kenv  = kenv - ioff

kfreq expon ifstart, idur, ifstop

aout  oscil  kenv, kfreq, 1

      out aout
      endin

      instr 3
iamp  = ampdbfs(p4)
ifstart = cpspch(p5)
ifstop  = cpspch(p6)
iavib   = p7
idur  = p3
ioff  = 0.01

kenv  expon iamp+ioff, idur, ioff
kenv  = kenv - ioff

kvibenv linseg 0,idur*0.05,0,idur*.95,iavib
kvib  oscil kvibenv, 4, 1
kfreq expon ifstart, idur, ifstop
kvib  = (kvib/2) + (kfreq/2)
kfreq = kfreq + kvib

display kfreq, idur

aout  oscil  kenv, kfreq, 1

      out aout
      endin

      instr 4
iamp  = ampdbfs(p4)
ifstart = cpspch(p5)
ifstop  = cpspch(p6)
iavib   = p7
idur  = p3
ioff  = 0.01
ivibs = 4
ivibe = 12

kenv  expon iamp+ioff, idur, ioff
kenv  = kenv - ioff

kvibenv linseg 0,idur*0.05,0,idur*.95,iavib
kvibf  expon ivibs,idur,ivibe
kvib  oscil kvibenv, kvibf, 1
kfreq expon ifstart, idur, ifstop
kvib  = (kvib/2) + (kfreq/2)
kfreq = kfreq + kvib

display kfreq, idur

aout  oscil  kenv, kfreq, 1

      out aout
      endin

      instr 5
iamp  = ampdbfs(p4)
ifstart = cpspch(p5)
ifstop  = cpspch(p6)
iavib   = p7
idur  = p3
ioff  = 0.01
ivibs = 2
ivibe = 5
idiff = 0.3418059

kenv  expon iamp+ioff, idur, ioff
kenv  = kenv - ioff

kvibenv linseg 0,idur*0.05,0,idur*.95,iavib
kvibf  expon ivibs,idur,ivibe
kvib  oscil kvibenv, kvibf, 1
kfreq expon ifstart, idur, ifstop
kvib  = (kvib/2) + (kfreq/2)
kfreq = kfreq + kvib

display kfreq, idur

a1    oscil  kenv, kfreq, 2
a2    oscil  kenv, kfreq+(idiff), 2
a3    oscil  kenv, kfreq+(2*idiff), 2
a4    oscil  kenv, kfreq+(3*idiff), 2
a5    oscil  kenv, kfreq+(4*idiff), 2
a6    oscil  kenv, kfreq+(5*idiff), 2

      out (a1+a2+a3+a4+a5+a6)/6
      endin
```
* [partitura](./20210315.sco)
```csound
f1 0 4096 10 1
f2 0 4096 10 1 0.5 0.333 0.24 0.2 0.166 0.142857  0.125000   0.111111   0.100000   0.090909   0.083333 0.076923   0.071429   0.066667   0.062500   0.058824   0.055556   0.052632 0.050000   0.047619   0.045455   0.043478   0.041667   0.040000   0.038462 0.037037   0.035714   0.034483   0.033333   0.032258   0.031250   0.030303

i1 0   3  -6  8.09

i2 4   0.5  -6  8.09 9.03
i2 5   0.5  -6  8.09 7.09

i3 6   2    -6  8.09 9.03 150

i4 8.5 4    -6  8.09 6.03 250

i5 13  8    -6  6.03 8.09 30
```
* tavole risultanti
  * ![3](./20210315-03.png)
  * ![4](./20210315-04.png)
  * ![5](./20210315-05.png)
* suoni risultanti
  * ![suoni risultanti](./20210315-out.png)
