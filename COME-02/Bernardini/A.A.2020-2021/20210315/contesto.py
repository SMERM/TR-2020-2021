from linear import Linear
class Contesto:
    def __init__(self, at, dur, ms, me):
        self.at = at
        self.dur = dur
        self.metro_start =ms
        self.metro_end=me
        self.tempofun=Linear(self.at,self.metro_start,self.at+self.dur,self.metro_end)

    def metro(self,now):
        return self.tempofun.y(now)

    def periodo(self,now):
        return 60 / self.metro(now)


C = Contesto(0, 60, 144,89)
