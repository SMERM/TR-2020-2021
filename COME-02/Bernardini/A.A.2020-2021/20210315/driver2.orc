

sr=48000
ksmps=10
nchnls=1
0dbfs=1

        instr 1
ifreq=cpspch(p4)
idur=p3
iatt=p2
iamp=1/6

kenv    expon  iamp, p3,    iamp/1000
aout    oscil  kenv, ifreq, 1

out aout
endin

