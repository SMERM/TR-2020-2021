# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 09/11/2020

### Argomenti

* Ricognizione dell'istituzione
* Strutturazione del corso di Musica Elettronica
  * suddivisione dei corsi
    * Composizione Musicale Elettroacustica
    * Informatica Musicale
    * Storia della Musica Elettroacustica
    * Esecuzione e interpretazione della Musica Elettroacustica
    * Elettroacustica
    * Acustica e psicoacustica
  * strumentazione logistica:
    * [calendario SME::Roma](https://calendar.google.com/calendar/u/0?cid=dG41bGs2NmQzcjdsazViaW1ucWxzb2Q1djBAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ)
    * [`git`](https://git.smerm.org/SMERM/TR-2020-2021)
    * [`slack`](https://smerm.slack.com) (dettaglio dei canali)
    * [`jitsi`](https://meet.smerm.org/come02_tr1)
    * [mailing list](https://groups.google.com/g/smerm-studenti)
    * [canale YouTube dedicato](https://www.youtube.com/channel/UCG7pgN812PK0nrAJZQ5FeVA)
* Chiarimenti sulle *funzioni* della musica
* Problematiche legate all'ascolto odierno: allenamento dell'orecchio
* Problematiche della Composizione Musicale Elettroacustica
  * collocazione nel contesto storico
    * la composizione dopo Cage
  * motivazioni delle scelte tecnologiche
  * l'attenzione per il timbro
  * una concezione olistica della composizione
  * comporre con spazi continui
  * astrazione compositiva, suggestioni narrative, sonificazioni, ecc.
* scelta delle tecnologie
  * riconsiderare il ruolo del computer (non più *appliance domestica* ma strumento di lavoro professionale)
  * funzionalità delle tecnologie
  * elementi necessari delle tecnologie 
  * strumenti di sintesi
    * [`csound`](https://csounds.com)
    * [`pure data`](http://pure-data.info)
    * [`SuperCollider`](https://supercollider.github.io/)
  * strumenti vari
    * [emulatore di terminale (`bash`)](https://www.gnu.org/software/bash/)
    * [editor di testo *puro ascii* (`atom`, tra i tanti)](https://atom.io/)
    * [editor di suoni - `audacity`](https://www.audacityteam.org/)
    * [analisi numerica - `octave`](https://www.gnu.org/software/octave/index)
    * [tipografia musicale - `lilypond`](http://lilypond.org/) con il suo *front-end* [`frescobaldi`](https://www.frescobaldi.org/index.html)
  * linguaggi di scripting
    * [`python`](https://python.org)
    * [`ruby`](https://www.ruby-lang.org/it/)

### Studio a casa

* allenamento dell'orecchio: due ore quotidiane di ascolto di [RadioTre Classica](https://www.raiplayradio.it/radioclassica/) decidendo quando e come distribuirle nella giornata ma **SENZA SCEGLIERE** in base al contenuto.
* scaricare, installare e verificare il funzionamento di tutti gli strumenti software sopra indicati
* studiare la prefazione e i capitoli 1-3 del [manuale di `csound`](http://floss.booktype.pro/csound/preface/)
* i primi quattro esercizi con `csound`:
  * una sinusoide a 443 Hz per 1 sec., ampiezza 10000 (verificare il file prodotto con `audacity`)
  * una sinusoide intermittente regolare (e.g. *telefono occupato*) per 10
    sec., ampiezza 10000 (verificare l'assenza di *clicks*, verificare il file prodotto con `audacity`)
  * una scala diatonica temperata dal do centrale (`261.6 Hz` o `8.00` in *pitch-class notation*) sino al do superiore
    eseguita in semicrome alternate a pause di semicrome a 120 mm la semiminima
  * una scala cromatica per quarti di tono temperati dal do centrale sino al
    do superiore eseguita in semicrome alternate a pause di semicrome a 120 mm la semiminima
