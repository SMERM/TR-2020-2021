# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 18/01/2021

## [Youtube video](https://youtu.be/YgkAY-AbFfk)

### Verifica dello studio a casa

* verifica consegne

### SuperCollider

* Architettura di `SuperCollider`:
  * libreria
* Hands-on:
  * interfacce di controllo
