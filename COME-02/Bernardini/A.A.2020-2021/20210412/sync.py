import yaml


class Suono:

    def __init__(self, conf):
        self.db = None
        with open(conf, 'r') as fh:
            self.db = yaml.load(fh)

    def to_csound(self):
        print("i1 0 %8.4f \"%s\"" % (self.db['suono_1']['durata'], self.db['suono_1']['path']))
        print("i2 0 %8.4f" %( self.db['suono_1']['durata'] ))
        print("i4 0 %8.4f" %( self.db['suono_1']['durata'] ))
        at = self.db['suono_1']['fase']
        step = (60/self.db['suono_1']['metro'])/2
        loc_dur = 0.1
        while at < self.db['suono_1']['durata']:
            print("i3 %8.4f %8.4f" % (at, loc_dur))
            at += step



print("f1 0 4096 10 1")
s = Suono('conf.yaml')
s.to_csound()
