# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 12/04/2021

## Elaborazione di struenti per brani concreti

* la sincronizzazine delle pulsazioni nei brani concreti
* `yaml` file di configurazione
* gli opcode `zak` di `csound`

### [`YAML` file di configurazione](./conf.yaml)

```yaml
#
# suono 1
#
# picco rilevato a: 33.086
# metronomo 121.61135039 la seminima
# a partire da .2765625
#
suono_1:
  path: 1934__rhumphries__rbh-train-freight-by.wav
  durata: 196
  metro: 121.61135039
  fase: .27656250
```

### [generatore `python` della partitura `csound`](./sync.py)

```python
import yaml


class Suono:

    def __init__(self, conf):
        self.db = None
        with open(conf, 'r') as fh:
            self.db = yaml.load(fh)

    def to_csound(self):
        print("i1 0 %8.4f \"%s\"" % (self.db['suono_1']['durata'], self.db['suono_1']['path']))
        print("i2 0 %8.4f" %( self.db['suono_1']['durata'] ))
        print("i4 0 %8.4f" %( self.db['suono_1']['durata'] ))
        at = self.db['suono_1']['fase']
        step = (60/self.db['suono_1']['metro'])/2
        loc_dur = 0.1
        while at < self.db['suono_1']['durata']:
            print("i3 %8.4f %8.4f" % (at, loc_dur))
            at += step



print("f1 0 4096 10 1")
s = Suono('conf.yaml')
s.to_csound()
```

### [orchestra `csound`](./sync.orc)

```csound
sr=48000
ksmps = 10
nchnls = 2
0dbfs = 1

      zakinit 1,1

      instr 1
ifile      = p4
al, ar     diskin2 ifile, 1

          outs al, ar
      endin

      instr 3
ifreq      = 880
idur       = p3
iamp       = ampdbfs(-4)

kfreq  zkr 0

kenv       expon iamp, idur, ampdbfs(-30)
aout       oscil kenv, ifreq+kfreq, 1

      zaw  aout, 0
      endin

      instr 4
iamp  = 0.5

kmod  oscil iamp, 3/p3, 1
kmod  = kmod + 0.5

aout    zar  0

      outs aout*kmod, aout*kmod
      endin

      instr 2
iamp  = 50

kmod  oscil iamp, 25/p3, 1
kmod  = kmod + iamp

      zkw  kmod, 0

      endin
```
