sr=48000
ksmps = 10
nchnls = 2
0dbfs = 1

      zakinit 1,1

      instr 1
ifile      = p4
al, ar     diskin2 ifile, 1

          outs al, ar
      endin

      instr 3
ifreq      = 880
idur       = p3
iamp       = ampdbfs(-4)

kfreq  zkr 0

kenv       expon iamp, idur, ampdbfs(-30)
aout       oscil kenv, ifreq+kfreq, 1

      zaw  aout, 0
      endin

      instr 4
iamp  = 0.5

kmod  oscil iamp, 3/p3, 1
kmod  = kmod + 0.5

aout    zar  0

      outs aout*kmod, aout*kmod
      endin

      instr 2
iamp  = 50

kmod  oscil iamp, 25/p3, 1
kmod  = kmod + iamp

      zkw  kmod, 0

      endin
