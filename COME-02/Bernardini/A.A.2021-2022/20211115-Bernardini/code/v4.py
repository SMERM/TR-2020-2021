#
# versione 4: segmenti completamente casuali (casualita` controllata)
#             da polvere di suono a suono per segmenti consequenziali
# tre file: 65508__juskiddink__flute1.wav
#           328727__hellska__flute-note-tremolo.wav
#           29860__herbertboland__circularbreathbassclarinet.wav
#
from random import random, randint

class Suono:
    def __init__(self, num, dur, instr):
       self.number = num
       self.dur = dur
       self.instrno = instr

    def location(self, value):
        return (value % self.dur)

def linear(t, y0, x0, y1, x1):
    a = (y1-y0)/(x1-x0)
    b = y0
    return a*t+b


totdur = 60 # secondi
leeway = 0.01 # overlap
halflw = leeway/2
now    = leeway  # variabile
files  = [ Suono(1, 98.133, 2), Suono(2, 4.733, 1), Suono(3, 56.639, 1) ]
startdur = 0.005  # secondi
enddur   = 0.8    # secondi
durrng   = 0.1    # percentuale di casualita` (10%)
skiprng  = 0.01   # percentuale di randomness

while(now < totdur):
    fileidx = randint(0, len(files)-1)
    suono = files[fileidx]
    file = suono.number
    fdur = suono.dur
    inst = suono.instrno
    dur_nominale = linear(now, startdur, 0, enddur, totdur)
    min_dur = dur_nominale * (1-durrng)
    max_dur = dur_nominale * (1+durrng)
    rng_dur = max_dur-min_dur
    dur = random()*rng_dur+min_dur # durata casuale (min_dur -> max_dur)
    amp = random()*-26-4          # ampiezza random tra -4 e -30 dB
    ourskip = now+(random()*skiprng*2-skiprng) # collocazione corrente...
    skip = suono.location(ourskip)  # ...modulo la durata del file
    print("i%d %8.4f %8.4f %+6.2f %1d %8.4f" % (inst, now-halflw, dur+leeway, amp, file, skip))
    now += dur
