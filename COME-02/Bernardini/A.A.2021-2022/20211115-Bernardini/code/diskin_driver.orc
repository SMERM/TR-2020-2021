;
; diskin driver per l'esercizio 1
;
; da modificare liberamente!
sr = 44100
ksmps=5
nchnls=2


          instr 1 ; mono input

idur      =   p3
iamp      =   ampdb(p4)
ifile     =   p5
iskip     =   p6
icorner   =   0.01

a1        diskin  ifile, 1, iskip
aout      linen   a1*iamp, icorner, idur, icorner

          outs    aout,aout

          endin

          instr 2 ; stereo input

idur      =   p3
iamp      =   ampdb(p4)
ifile     =   p5
iskip     =   p6
icorner   =   0.01

al,ar     diskin  ifile, 1, iskip
al        linen   al*iamp, icorner, idur, icorner
ar        linen   ar*iamp, icorner, idur, icorner

          out     al, ar

          endin

          instr 3 ; mono input

idur      =   p3
iamp      =   ampdb(p4)
ifile     =   p5
iskip     =   p6
icorner   =   0.01
ipitch    =   p7    ; 2 -> ottava sopra, 0.5 -> ottava sotto

a1        diskin  ifile, ipitch, iskip
aout      linen   a1*iamp, icorner, idur, icorner

          outs    aout,aout

          endin

          instr 4 ; stereo input

idur      =   p3
iamp      =   ampdb(p4)
ifile     =   p5
iskip     =   p6
icorner   =   0.01
ipitch    =   p7    ; 2 -> ottava sopra, 0.5 -> ottava sotto

al,ar     diskin  ifile, ipitch, iskip
al        linen   al*iamp, icorner, idur, icorner
ar        linen   ar*iamp, icorner, idur, icorner

          out     al, ar

          endin
