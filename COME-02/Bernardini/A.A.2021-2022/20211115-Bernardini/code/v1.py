#
# versione 1: segmenti completamente casuali (casualita` controllata)
# un solo file: 65508__juskiddink__flute1.wav                   (98.133 secondi)
#
from random import random

totdur = 30 # secondi
leeway = 0.01 # overlap
hlee   = leeway/2
now    = leeway  # variabile
file   = 1  # soundin.1
instr  = 2  # instrument 2
fdur   = 98.133 # durata del file

while(now < totdur):
    dur = random()*0.79+0.01      # durata casuale (10 msec-800 msec)
    amp = random()*-38-2          # ampiezza random tra -2 e -40 dB
    skip = random()*(fdur-dur)    # skip tra 0 e durata del file - la durata della nota
    print("i%d %8.4f %8.4f %+6.2f %1d %8.4f" % (instr, now-hlee, dur+leeway, amp, file, skip))
    now += dur
