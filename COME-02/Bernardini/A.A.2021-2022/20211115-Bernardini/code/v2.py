#
# versione 2: segmenti completamente casuali (casualita` controllata)
# tre file: 65508__juskiddink__flute1.wav
#           328727__hellska__flute-note-tremolo.wav
#           29860__herbertboland__circularbreathbassclarinet.wav
#
from random import random, randint

totdur = 30 # secondi
now    = 0  # variabile
leeway = 0.01 # overlap
files  = [1, 2, 3]  # soundin.1
fdurs  = [98.133, 4.733, 56.639] # durata del file
finss  = [2, 1, 1]

while(now < totdur):
    fileidx = randint(0, 2)
    file = files[fileidx]
    fdur = fdurs[fileidx]
    fins = finss[fileidx]
    dur = random()*0.79+0.01      # durata casuale (10 msec-800 msec)
    amp = random()*-38-2          # ampiezza random tra -2 e -40 dB
    skip = random()*(fdur-dur)    # skip tra 0 e durata del file - la durata della nota
    print("i%d %8.4f %8.4f %+6.2f %1d %8.4f" % (fins, now, dur, amp, file, skip))
    now += dur
