;
; esempio di partitura manuale
;
; il suono 1 è 65508__juskiddink__flute1.wav                   (98.133 secondi)
; il suono 2 è 328727__hellska__flute-note-tremolo.wav         (4.733 seconds)
; il suono 3 è 29860__herbertboland__circularbreathbassclarinet.wav (56.639 seconds)
;
; presi da https://freesound.org

i2 0 1 -8 1 10.5
i1 0.5 0.75 -4 2 3.5 
i1 1.5 0.25 -8 3 15.5
i1 1.5 0.75 -2 2 2.2
