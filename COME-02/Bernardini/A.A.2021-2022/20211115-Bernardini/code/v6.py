#
# versione 6: segmenti ritmici e altezze ascendenti
# tre file: 65508__juskiddink__flute1.wav
#           328727__hellska__flute-note-tremolo.wav
#           29860__herbertboland__circularbreathbassclarinet.wav
#
from random import random, randint

class Suono:
    def __init__(self, num, dur, instr):
       self.number = num
       self.dur = dur
       self.instrno = instr

    def location(self, value):
        return (value % self.dur)

def linear(t, y0, x0, y1, x1):
    a = (y1-y0)/(x1-x0)
    b = y0
    return a*t+b

def dur_figura(metro, seq, idx):
    ridx = idx % len(seq)
    pulse = 60.0/float(metro)
    return (4*seq[ridx]*pulse)

ritmo = [1/24.0,1/24.0,1/24.0,1/24.0,1/24.0,1/24.0,1/4.0,1/4.0]


totdur = 60 # secondi
leeway = 0.01 # overlap
now    = leeway    # il tempo attuale
halflw = leeway/2
files  = [ Suono(1, 98.133, 4), Suono(2, 4.733, 3), Suono(3, 56.639, 3) ]
startdur = 0.005  # secondi
enddur   = 0.8    # secondi
durrng   = 0.1    # percentuale di casualita` (10%)
skiprng  = 0.01   # percentuale di randomness
metro    = 136
pstart   = 0.5
pend     = 2.0

idx = 0
while(now < totdur):
    fileidx = randint(0, len(files)-1)
    suono = files[fileidx]
    file = suono.number
    fdur = suono.dur
    inst = suono.instrno
    dur = dur_figura(metro, ritmo, idx)
    amp = random()*-26-4          # ampiezza random tra -4 e -30 dB
    ourskip = now+(random()*skiprng*2-skiprng) # collocazione corrente...
    skip = suono.location(ourskip)  # ...modulo la durata del file
    pitch = linear(now, pstart, 0, pend, totdur)
    print("i%d %8.4f %8.4f %+6.2f %1d %8.4f %8.4f" % (inst, now-halflw, dur+leeway, amp, file, skip, pitch))
    now += dur
    idx += 1
