#
# versione 2: segmenti completamente casuali (casualita` controllata)
#             versione (object oriented)
# tre file: 65508__juskiddink__flute1.wav
#           328727__hellska__flute-note-tremolo.wav
#           29860__herbertboland__circularbreathbassclarinet.wav
#
from random import random, randint

class Suono:
    def __init__(self, num, dur, instr):
       self.number = num
       self.dur = dur
       self.instrno = instr

totdur = 30 # secondi
now    = 0  # variabile
leeway = 0.01 # overlap
files  = [ Suono(1, 98.133, 2), Suono(2, 4.733, 1), Suono(3, 56.639, 1) ]

while(now < totdur):
    fileidx = randint(0, 2)
    suono = files[fileidx]
    file = suono.number
    fdur = suono.dur
    inst = suono.instrno
    dur = random()*0.79+0.01      # durata casuale (10 msec-800 msec)
    amp = random()*-38-2          # ampiezza random tra -2 e -40 dB
    skip = random()*(fdur-dur)    # skip tra 0 e durata del file - la durata della nota
    print("i%d %8.4f %8.4f %+6.2f %1d %8.4f" % (inst, now, dur, amp, file, skip))
    now += dur
