# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 24/01/2022

### Argomenti

#### Esercizio 1: discussione dei singoli progetti

* realizzazione pratica dei progetti riguardanti l'esercizio 1

#### Ascolto e analisi del brano *Musica su Due Dimensioni* (1958) - Bruno Maderna

* Analisi strutturale dei gruppi iniziali
  * cellula iniziale: 3-0: [0,1,2]
