from math import cos,pi
from random import randint

class Suono:
    def __init__(self, filename, dur, instrno = 1, sr = 44100):
        self.name = filename
        self.amp  = self.compute_amp()
        self.dur  = dur
        self.instrno = instrno
        self.sampling_rate = sr

    def compute_amp(self):
        return 0

class Hanning:
    def __init__(self, dur):
        self.dur = dur

    def y(self, t):
        return -0.5*cos(2*pi/self.dur*t)+0.5


class Processo:
    def __init__(self, dur, offset = 0):
        self.dur = dur
        self.offset = 0

    def run(self):
        self.header()
        self.__run__()
        self.trailer()

    def __run__(self):
        pass

    def header(self):
        print(";\n; inizio processo\n;")

    def trailer(self):
        print(";\n; fine processo\n;")

class Randomizzatore(Processo):
    def __init__(self, suono, offset = 0, tabsize = 8192, randrange_perc = 10, amp = 0):
        self.suono = suono
        super().__init__(self.suono.dur, offset)
        self.tabsize = tabsize
        self.randrange_perc = randrange_perc # la randomness sulle finestre in *percentuale*
        self.amp = 0

    def header(self):
        print(";\nf1 0 %d 20 2 1\n;\n;" % (self.tabsize))

    def __run__(self):
        now = self.offset
        idx = 0
        h = Hanning(self.suono.dur)
        randrange = int(((self.suono.dur*self.suono.sampling_rate)/float(self.tabsize))*(self.randrange_perc/100.0))
        graindur = self.tabsize/self.suono.sampling_rate
        hopsize = self.tabsize/4
        hoptime = hopsize/self.suono.sampling_rate
        
        while (now < self.suono.dur):
            randomness = h.y(now)
            winskip = int(randint(-randrange, randrange)*randomness)
            winnum   = idx + winskip
            skiptime = winnum * hoptime
            print("i%d %8.4f %8.4f %+6.2f \"%s\" %8.4f ; %d" % (self.suono.instrno, now, graindur, self.amp, self.suono.name, skiptime, winnum))
            now += hoptime
            idx += 1
