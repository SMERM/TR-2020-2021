sr=44100
ksmps=1
nchnls=1



      instr 1
ifile = p5
iamp  = ampdb(p4)
idelay = p6/sr     ; ritardo in campioni

aout  diskin2 ifile, 1, 0
adel1 delayr  idelay

      delayw   aout

adel2 delayr  idelay/2

      delayw  adel1

      out  (aout+adel1+adel2)*iamp

      endin

      instr 2
idur  = p3
ifile = p5
iamp  = ampdb(p4)
iskip = p6

aout  diskin2 ifile, 1, iskip
kwin  oscil1i 0, 1, idur, 1

      out    aout*kwin*iamp
      endin

      instr 3
idur  = p3
ifile = p5
iamp  = ampdb(p4)
iskip = p6

al,ar diskin2 ifile, 1, iskip
kwin  oscil1i 0, 1, idur, 1
aout  = (al+ar)/2

      out    aout*kwin*iamp
      endin
