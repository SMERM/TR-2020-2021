from math import cos,pi
from random import randint

sr = 44100.0
tabsize = 8192
hopsize = tabsize/4
hoptime = hopsize/sr
graindur = tabsize/sr
sounddur = 98.1
randrange = 200
amp     = 0
filename = "65508__juskiddink__flute1.wav"
instrno = 3

class Hanning:
    def __init__(self, dur):
        self.dur = dur

    def y(self, t):
        return -0.5*cos(2*pi/self.dur*t)+0.5



print("f1 0 %d 20 2 1\n\n" % (tabsize))

now = 0
idx = 0
h = Hanning(sounddur)

while (now < sounddur):
    randomness = h.y(now)
    winskip = int(randint(-randrange, randrange)*randomness)
    winnum   = idx + winskip
    skiptime = winnum * hoptime
    print("i%d %8.4f %8.4f %+6.2f \"%s\" %8.4f ; %d" % (instrno, now, graindur, amp, filename, skiptime, winnum))
    now += hoptime
    idx += 1
