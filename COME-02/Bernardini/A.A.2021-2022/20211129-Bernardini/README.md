# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 29/11/2021

### Argomenti

#### Esercizio 1: discussione dei singoli progetti

* Esposizione delle singole idee progettuali

#### Esercizio 1: Strumentario

###### Esempio di `orchestra`

[driver.orc](./code/driver.orc)
```csound
sr=44100
ksmps=1
nchnls=1



      instr 1
ifile = p5
iamp  = ampdb(p4)
idelay = p6/sr     ; ritardo in campioni

aout  diskin2 ifile, 1, 0
adel1 delayr  idelay

      delayw   aout

adel2 delayr  idelay/2

      delayw  adel1

      out  (aout+adel1+adel2)*iamp

      endin

      instr 2
idur  = p3
ifile = p5
iamp  = ampdb(p4)
iskip = p6

aout  diskin2 ifile, 1, iskip
kwin  oscil1i 0, 1, idur, 1

      out    aout*kwin*iamp
      endin

      instr 3
idur  = p3
ifile = p5
iamp  = ampdb(p4)
iskip = p6

al,ar diskin2 ifile, 1, iskip
kwin  oscil1i 0, 1, idur, 1
aout  = (al+ar)/2

      out    aout*kwin*iamp
      endin
```

#### [v3.py](./code/v3.py)

```python
from math import cos,pi
from random import randint

sr = 44100.0
tabsize = 8192
hopsize = tabsize/4
hoptime = hopsize/sr
graindur = tabsize/sr
sounddur = 98.1
randrange = 200
amp     = 0
filename = "65508__juskiddink__flute1.wav"
instrno = 3

class Hanning:
    def __init__(self, dur):
        self.dur = dur

    def y(self, t):
        return -0.5*cos(2*pi/self.dur*t)+0.5



print("f1 0 %d 20 2 1\n\n" % (tabsize))

now = 0
idx = 0
h = Hanning(sounddur)

while (now < sounddur):
    randomness = h.y(now)
    winskip = int(randint(-randrange, randrange)*randomness)
    winnum   = idx + winskip
    skiptime = winnum * hoptime
    print("i%d %8.4f %8.4f %+6.2f \"%s\" %8.4f ; %d" % (instrno, now, graindur, amp, filename, skiptime, winnum))
    now += hoptime
    idx += 1
```

### [v4.py](./code/v4.py)

```python
import v4lib

suono = v4lib.Suono("65508__juskiddink__flute1.wav", 98.1, instrno = 3, sr = 44100)
processo = v4lib.Randomizzatore(suono) 

processo.run()
```

Questa realizzazione è basata sulla [seguente libreria](./code/v4lib.py):

```python
from math import cos,pi
from random import randint

class Suono:
    def __init__(self, filename, dur, instrno = 1, sr = 44100):
        self.name = filename
        self.amp  = self.compute_amp()
        self.dur  = dur
        self.instrno = instrno
        self.sampling_rate = sr

    def compute_amp(self):
        return 0

class Hanning:
    def __init__(self, dur):
        self.dur = dur

    def y(self, t):
        return -0.5*cos(2*pi/self.dur*t)+0.5


class Processo:
    def __init__(self, dur, offset = 0):
        self.dur = dur
        self.offset = 0

    def run(self):
        self.header()
        self.__run__()
        self.trailer()

    def __run__(self):
        pass

    def header(self):
        print(";\n; inizio processo\n;")

    def trailer(self):
        print(";\n; fine processo\n;")

class Randomizzatore(Processo):
    def __init__(self, suono, offset = 0, tabsize = 8192, randrange_perc = 10, amp = 0):
        self.suono = suono
        super().__init__(self.suono.dur, offset)
        self.tabsize = tabsize
        self.randrange_perc = randrange_perc # la randomness sulle finestre in *percentuale*
        self.amp = 0

    def header(self):
        print(";\nf1 0 %d 20 2 1\n;\n;" % (self.tabsize))

    def __run__(self):
        now = self.offset
        idx = 0
        h = Hanning(self.suono.dur)
        randrange = int(((self.suono.dur*self.suono.sampling_rate)/float(self.tabsize))*(self.randrange_perc/100.0))
        graindur = self.tabsize/self.suono.sampling_rate
        hopsize = self.tabsize/4
        hoptime = hopsize/self.suono.sampling_rate
        
        while (now < self.suono.dur):
            randomness = h.y(now)
            winskip = int(randint(-randrange, randrange)*randomness)
            winnum   = idx + winskip
            skiptime = winnum * hoptime
            print("i%d %8.4f %8.4f %+6.2f \"%s\" %8.4f ; %d" % (self.suono.instrno, now, graindur, self.amp, self.suono.name, skiptime, winnum))
            now += hoptime
            idx += 1
```

### Lavagna usata durante la lezione

![whiteboard](./whiteboard_20211129.png)

### Studio a casa

* realizzazione pratica dei progetti riguardanti l'esercizio 1
