
import numpy as np
import matplotlib.pyplot as plt
import polyfactors as pf

xfun = np.arange(-1, 9, 0.001)


#
# y = fact[0]*x^n + fact[1]*x^(n-1) + ....
#
def create_y(x, fact):
    nfacts = len(fact)-1
    nf = nfacts
    result = np.zeros(len(x), dtype='float64')
    print(x.shape)
    print(result.shape)
    for f in fact:
        result += (f*x**(float(nf)))
        nf -= 1
    return result

yfun3 = create_y(xfun, pf.poly3)
yfun2 = create_y(xfun, pf.poly2)
yfun1 = create_y(xfun, pf.poly1)
plt.plot(xfun, yfun3, xfun, yfun2, xfun, yfun1)
plt.show()
