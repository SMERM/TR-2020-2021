punti = [ 0.5, 0.7; 1.3, -2.4; 6.7, 3.2; 8.9, 0.001 ];
X = punti(:,1);
Y = punti(:,2);
p3 = polyfit(X, Y, 3);
p2 = polyfit(X, Y, 2);
p1 = polyfit(X, Y, 1);

xfun = [-1:0.001:9];
yfun3 = polyval(p3, xfun); % y = p(1)*x^3 + p(2)*x^2 + p(3)*x^1 + p(4)*x^0
yfun2 = polyval(p2, xfun);
yfun1 = polyval(p1, xfun);

% plot(X,Y, '*', xfun, yfun3, ';terzo grado;', xfun, yfun2, ';secondo grado;', xfun, yfun1, ';primo grado;')

printf("poly3 = [")
for n=1:length(p3)
  printf("%f, ", p3(n));
end
printf("]\n");
printf("poly2 = [")
for n=1:length(p2)
  printf("%f, ", p2(n));
end
printf("]\n");
printf("poly1 = [")
for n=1:length(p1)
  printf("%f, ", p1(n));
end
printf("]\n");
