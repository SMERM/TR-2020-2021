
class Drawing:
    def __init__(self, width, height, offset = 0.5, f0 = 261.6, maxfreq = 3000):
        self.width = width
        self.height = height
        self.offset = offset
        self.f0 = f0
        self.maxfreq = maxfreq

    def header(self):
        print(".PS\nscale=2.54\noffset=0.5\nFrame: box invis wid %4.2f ht %4.2f\nAsseX: line from Frame.sw + (0,%4.2f) to Frame.se + (0,%4.2f)\nAsseY: line from Frame.sw + (%4.2f,0) to Frame.nw + (%4.2f,0)" % (self.width, self.height, self.offset, self.offset, self.offset, self.offset))

    def trailer(self):
        print(".PE")

    def draw(self):
        self.header()
        hzinc = (self.height - self.offset)/self.maxfreq
        curfreq = self.f0
        end = self.maxfreq
        idx = 0
        while(curfreq < end):
            curfreq = self.f0 * 2**(idx/12) 
            curfrqpos = curfreq * hzinc
            cfp = curfrqpos+self.offset
            print("line from Frame.sw + (0, %4.2f) to Frame.se + (0, %4.2f) \"\\s[-8]%4.2f\\s[0]\" above" % (cfp, cfp, curfreq))
            idx+=1
        self.trailer()


d = Drawing(10, 20)
d.draw()
