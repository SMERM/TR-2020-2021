# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 08/11/2021

### Argomenti

* Introduzione ai 5 esercizi da svolgere durante l'anno
* [REVISIONE] Chiarimenti sulle *funzioni* (vs. i *generi*) della musica
* [REVISIONE] Problematiche legate all'ascolto odierno: allenamento dell'orecchio
* Problematiche delle composizioni per strumento/i e *dead electronics*
  * scrivere per strumento:
    * conoscenza "tradizionale" dello strumento
    * tecniche strumentali estese
    * aumentazione dello strumento
  * musica a ≥ 2 voci
  * relazione tra le voci
    * mimesi
    * dialogo
    * indifferenza
    * contrapposizione
  * problematiche tecnico/esecutive
    * sincronia tra le parti
    * costrizione (o meno) dell'interpretazione strumentale
* Problematiche della scrittura *elettroacustica* di uno strumento tradizionale
  * come cambia il pensiero strumentale con l'aggiunta elettroacustica
  * scrittura algoritmica per strumento
  * comporre col suono
  * considerazioni sulla *musicalità* delle scelte
* scelta delle tecnologie
  * strumenti di sintesi
    * [`csound`](https://csounds.com)
    * [`pure data`](http://pure-data.info)
    * [`SuperCollider`](https://supercollider.github.io/)
  * strumenti vari
    * [emulatore di terminale (`bash`)](https://www.gnu.org/software/bash/)
    * [editor di testo *puro ascii* (`atom`, tra i tanti)](https://atom.io/)
    * [editor di suoni - `audacity`](https://www.audacityteam.org/)
    * [analisi numerica - `octave`](https://www.gnu.org/software/octave/index)
    * [tipografia musicale - `lilypond`](https://lilypond.org/) con il suo *front-end* [`frescobaldi`](https://www.frescobaldi.org/index.html)
    * [tipografia musicale - `pic`](https://it.wikipedia.org/wiki/Groff_(software))
  * linguaggi di scripting
    * [`python`](https://python.org)
    * [`ruby`](https://www.ruby-lang.org/it/)

### Studio a casa

* abbozzo del progetto dell'esercizo 1
