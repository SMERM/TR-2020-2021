# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 11/04/2022

[Video Youtube](https://youtu.be/k7rVetG3Lmw)

### Argomenti

#### Transient-synthesis

![Transient synthesis](./transient+synthesis.png)

#### Introduzione alla localizzazione spaziale dei suoni (2)

![prime riflessioni](./prime_riflessioni.png)

![spazializzatore csound](./spazializzatore_csound.png)


```csound
;
; - leggere il segnale
; - leggere i dati della stanza
; - leggere posizioni altoparlanti
;
; - calcolare i diretti per ogni altoparlante
; - calcolare le prime riflessioni sulle pareti
; - aggiungere all pass
;

  instr 1000, 1001, 1002, 1003, 1004

  indexoffset = 1000
  index = p1 - indexoffset
  itable = 1000
  ;
  ; coordinate del suono
  ;
  ix = p4
  iy = p5
  ;
  ; leggere il segnale
  ;
asig           zar     index
  ;
  ; leggere i dati della stanza
  ;
iupleftx      table  0, itable
iuplefty      table  1, itable
idwritex      table  2, itable
idwritey      table  3, itable
  ;
  ; posizioni degli altoparlanti
  ;
ispklx        table  4, itable
ispkly        table  5  itable
ispkrx        table  6, itable
ispkry        table  7  itable

;
; TO BE CONTINUED....
;


              zacl     index 
   endin
```

```csound
;
; coordinate della stanza e posizioni degli altoparlanti
;
;             coord. angoli stanza                       coor. altoparlanti
;      (sinistra fronte, destra dietro)
;              |  x y x  y  |                            |  x  y  x  y  |
f1000 0 16 -2    -4 4 4 -1                                 -1  1  1  1
```
