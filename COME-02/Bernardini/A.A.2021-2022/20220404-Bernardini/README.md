# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 04/04/2022

[Video Youtube](https://youtu.be/QkQQBOwHDiw)

### Argomenti

#### Il opcode di `csound` `sndwarp`

![sndwarp](./sndwarp.png)

#### Introduzione alla localizzazione spaziale dei suoni

![localizzazione dei suoni](./localizzazione_dei_suoni.png)
