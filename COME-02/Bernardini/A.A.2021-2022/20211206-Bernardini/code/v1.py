from random import randint, random
import math



class Processo:
    def __init__(self, totdur = 30, metro = 96, ritmo = [1/4.0], distanze = [1, 100]):
        self.totdur = totdur
        self.metro = metro
        self.ritmo = ritmo
        self.distanze = distanze
        self.a = (self.distanze[1] - self.distanze[0])/self.totdur
        self.b = self.distanze[0]
    def dove(self, now):
        return self.a*now+self.b
    def header(self):
        print("f1 0 4096 10 1")
    def __run__(self):
        passo = 60.0/self.metro
        now = 0.05
        idx = 0

        while (now < self.totdur):
            grain = randint(5, 15)
            prossimo = self.ritmo[idx]*4*passo
            distanza = self.dove(now)
            for g in range(grain):
                at = (random()*0.05-0.025)+now
                freq = random()*300+150
                dur = random()*0.03+0.015
                amp = (10**((((math.fabs(at-now))*(-8/.025))-8)/20))/distanza
                ampdb = 20*math.log10(amp)
                print("i1 %8.4f %8.4f %+6.2f %8.4f ;%d di %d" % (at, dur, ampdb, freq, g, grain))
            now += prossimo
            idx += 1
            idx %= len(self.ritmo)
    def run(self):
        self.header()
        self.__run__()


if __name__ == "__main__":
    p1 = Processo()
    p2 = Processo(metro = 34, ritmo = [1/4.0, 1/16.0, 1/16.0, 1/16.0, 1/16.0, 1/8.0], distanze = [100, 1])
    p1.run()
    p2.run()
