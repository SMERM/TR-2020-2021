sr = 48000
ksmps = 5
nchnls = 1

instr 1
iamp = ampdbfs (p4)
ifreq = p5
idur = p3


  a1 oscil 1, ifreq, 1
  kenv expon iamp, idur, iamp*0.001

out a1*kenv
endin
