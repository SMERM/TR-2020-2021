# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 07/02/2022

[YouTube video](https://youtu.be/L3Z9XT0dZJQ)

### Argomenti

#### Esercizio 1: discussione dei singoli progetti

* realizzazione pratica dei progetti riguardanti l'esercizio 1 (Mauro Di Giovanni)

Versione non strutturata:

```python
#
# versione 3: segmenti completamente casuali (casualita` controllata)
#             da polvere di suono a suono
# tre file: 65508__juskiddink__flute1.wav
#           328727__hellska__flute-note-tremolo.wav
#           29860__herbertboland__circularbreathbassclarinet.wav
#
from random import random, randint

class Suono:
    def __init__(self, num, dur, instr=1):
       self.number = num
       self.dur = dur
       self.instrno = instr

#Classe chiamabile
class Linear:
    def __init__(self, y0, x0, y1, x1):
        self.a = (y1-y0)/(x1-x0)
        self.b = y0-(self.a*x0)
    def __call__(self, t):
        return self.a*t+self.b
class Pesata:
    def __init__(self, y0, x0, y1, x1):
        self.lin = Linear(y0, x0, y1, x1)
    def __call__(self, t):
        peso = self.lin(t)
        rnd = random()
        result = False
        if rnd < peso:
            result = True
        return result



totdur = 30 # durata della composizione
now    = 0  # variabile
leeway = 0.01 # overlap
halflw = leeway/2
files  = [ Suono(1, 98.133)]#, Suono(2, 4.733, 1), Suono(3, 56.639, 1) ] #creo tre oggetti  in cui indico quale file audio, la durata del file audio, quale instrument deve leggerlo
startdur = 0.005  # minimo valore della durata della "nota"
enddur   = 0.8    # massimo valore della durata della "nota"
durrng   = 0.3    # percentuale di casualita`

ldur = Linear(startdur, 0, enddur, totdur)
faipausa = Pesata(0,0,0.85, totdur)

while(now < totdur):
    fileidx = 0 # randint(0, 2) # prendo uno dei tre file audio a caso
    suono = files[fileidx] # metto nella variabile suono l'istanza dell'array files indicata da fileidx
    file = suono.number # metto nella variabile file il numero del file scelto in fileidx
    fdur = suono.dur # idem con dur
    inst = suono.instrno # idem con instrno
    #dur_nominale = linear(now, startdur, 0, enddur, totdur) # dur_nominale avrà un valore volta per volta calcolato dalla funzione linear data da now=t(unica variabile) al tempo=0 che avrà valori che cresceranno via via compresi tra un minimo (stardur) ed un massimo (endur)
    dur_nominale = ldur(now)
    min_dur = dur_nominale * (1-durrng) # min_dur sarà il risultato del dur_nominale motiplicato dalla percentuale di casualità sottratta a 1
    max_dur = dur_nominale * (1+durrng) # max_dur sarà il risultato del dur_nominale motiplicato dalla percentuale di casualità sommata a 1
    rng_dur = max_dur-min_dur # il range della durata avrà un valore compreso tra la durata minima e quella massima
    dur = random()*rng_dur+min_dur # durata casuale (min_dur -> max_dur)
    amp = random()*-26-4          # ampiezza random tra -4 e -30 dB
    skip = random()*(fdur-dur)    # skip tra 0 e durata del file - la durata della nota
    if faipausa(now):
        print(";\n; pausa \n;")
    else:
        print("i%d %8.4f %8.4f %+6.2f %1d %8.4f" % (inst, now-halflw, dur+leeway, amp, file, skip)) # p1 (strumento) p2 (inizio nota) p3 (durata della nota) p4 (ampiezza) p5 (file audio) p6 (punto di inzio della lettura)
    now += dur
```

Versione strutturata:

```python
#
# versione 3: segmenti completamente casuali (casualita` controllata)
#             da polvere di suono a suono
# tre file: 65508__juskiddink__flute1.wav
#           328727__hellska__flute-note-tremolo.wav
#           29860__herbertboland__circularbreathbassclarinet.wav
#
from random import random, randint

class Suono:
    def __init__(self, num, dur, instr=1):
       self.number = num
       self.dur = dur
       self.instrno = instr

#Classe chiamabile
class Linear:
    def __init__(self, y0, x0, y1, x1):
        self.a = (y1-y0)/(x1-x0)
        self.b = y0-(self.a*x0)
    def __call__(self, t):
        return self.a*t+self.b
class Pesata:
    def __init__(self, y0, x0, y1, x1):
        self.lin = Linear(y0, x0, y1, x1)
    def __call__(self, t):
        peso = self.lin(t)
        rnd = random()
        result = False
        if rnd < peso:
            result = True
        return result

class Processo:
    def __init__(self, totdur, files, startdur, enddur, durrng):
        self.totdur = totdur # durata della composizione
        self.now    = 0  # variabile
        self.leeway = 0.01 # overlap
        self.halflw = self.leeway/2
        self.files  = files #, Suono(2, 4.733, 1), Suono(3, 56.639, 1) ] #creo tre oggetti  in cui indico quale file audio, la durata del file audio, quale instrument deve leggerlo
        self.startdur = startdur  # minimo valore della durata della "nota"
        self.enddur   = enddur    # massimo valore della durata della "nota"
        self.durrng   = durrng    # percentuale di casualita`
        self.ldur = Linear(startdur, 0, enddur, totdur)
        self.faipausa = Pesata(0,0,0.85, totdur)
    def run (self):
        while(self.now < self.totdur):
            fileidx = 0 # randint(0, 2) # prendo uno dei tre file audio a caso
            suono = self.files[fileidx] # metto nella variabile suono l'istanza dell'array files indicata da fileidx
            file = suono.number # metto nella variabile file il numero del file scelto in fileidx
            fdur = suono.dur # idem con dur
            inst = suono.instrno # idem con instrno
            #dur_nominale = linear(now, startdur, 0, enddur, totdur) # dur_nominale avrà un valore volta per volta calcolato dalla funzione linear data da now=t(unica variabile) al tempo=0 che avrà valori che cresceranno via via compresi tra un minimo (stardur) ed un massimo (endur)
            dur_nominale = self.ldur(self.now)
            min_dur = dur_nominale * (1-self.durrng) # min_dur sarà il risultato del dur_nominale motiplicato dalla percentuale di casualità sottratta a 1
            max_dur = dur_nominale * (1+self.durrng) # max_dur sarà il risultato del dur_nominale motiplicato dalla percentuale di casualità sommata a 1
            rng_dur = max_dur-min_dur # il range della durata avrà un valore compreso tra la durata minima e quella massima
            dur = random()*rng_dur+min_dur # durata casuale (min_dur -> max_dur)
            amp = random()*-26-4          # ampiezza random tra -4 e -30 dB
            skip = random()*(fdur-dur)    # skip tra 0 e durata del file - la durata della nota
            if self.faipausa(self.now):
                print(";\n; pausa \n;")
            else:
                print("i%d %8.4f %8.4f %+6.2f %1d %8.4f" % (inst, self.now-self.halflw, dur+self.leeway, amp, file, skip)) # p1 (strumento) p2 (inizio nota) p3 (durata della nota) p4 (ampiezza) p5 (file audio) p6 (punto di inzio della lettura)
            self.now += dur
#startdur, enddur, durrng
p = Processo(30, [Suono(2, 4.733, 1)], 0.5, 0.1, 0.3)

p.run()
```
