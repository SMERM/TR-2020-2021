# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini

## Programma di studio generale (classe 2020-2021)

### [Introduzione](./A.A.2020-2021/20201109/README.md)

### Struttura e cronologia degli argomenti

Al di là dell'introduzione sopracitata  l'elenco  degli  argomenti  non
segue un ordine  cronologico.  Tutti  gli  argomenti  dovrebbero  essere
affrontati contemporaneamente in un approfondimento progressivo *a cipolla*.
Questo consente di elaborare subito degli esercizi e lo studio a casa.
La progressione degli argomenti segue quindi un andamento a spirale, entrando
via via nelle porzioni più avanzate di ciascun argomento.

### Materiali

* suoni sintetici
  * sinusoidi singole
  * suoni composti
    * principi di fusione spettrale
* suoni concreti
  * estrazione di caratteristiche
    * centroide
    * flusso spettrale

### Strategie di organizzazione parametrica

#### Organizzazioni casuali

* principi di casualità
  * distribuzione uniforme
  * distribuzione uniforme delimitata
  * distribuzione gaussiana
  * distribuzioni caotiche
  * distribuzioni frattali
* musicalità della casualità

#### Organizzazioni seriali

* serie parametriche
  * trasformazioni
  * traslazioni
  * rotazioni
  * permutazioni

#### Organizzazioni gerarchiche

* principi oppositivi
* sostituzioni gerarchiche
* principi contrappuntistici
  * imitazioni
  * canoni
  * fugati

#### Organizzazioni eterogenee

* funzioni di controllo
  * lineari
  * esponenziali
  * polinomiali
* conversione stimoli visivi -> stimoli uditivi
* sonificazione

### Gli spazi parametrici della composizione

#### Frequenza

* distribuzioni frequenziali
  * algoritmi distributivi
  * distribuzioni scalari
  * distribuzioni spettrali
  * distribuzioni psicoacusticamente informate
  * lo spettro come *real estate*
  * valenze estetiche
* sovrapposizioni frequenziali
  * banda critica
  * dissonanza e consonanza psicoacustiche

#### Tempo

* segmentazione convenzionale
  * segmentazione ritmica
    * nomenclature
    * ritmo, metro
    * accenti 
* segmentazione non convenzionale
  * distribuzione continua
  * distribuzione caotica
    * attrattori
* funzioni continue:
  * accelerandi e decelerandi
    * lineari
    * esponenziali
    * altre funzioni non lineari

#### Timbro

* armonicità vs inarmonicità
* rumore
  * bianco
  * rosa
* timbri naturali
  * timbri musicali
  * timbri concreti
* timbri artificiali
  * musicalità delle tecniche di sintesi:
    * sintesi additiva
    * sintesi sottrattiva
    * modulazioni
    * granulazioni
    * sintesi incrociate
  * distribuzioni spettrali
    * constraints psicoacustiche
    * distribuzioni regolari
    * distribuzioni irregolari
  * morphing timbrico
* silenzio: tipologie
  * relatività del silenzio
  * modalità del silenzio
    * silenzio ritmico
    * silenzio aritmico

#### Dinamica

* Relazione dinamica/energia spettrale
* Relazione dinamica/ampiezza
* Crescendo/Decrescendo
  * lineare
  * esponenziale
* *Empfindsamkeit* dinamica

#### Forma

* Rilievi storici della forma
* Recupero di forme tradizionali
* Forme astratte
  * suggestioni geometriche
  * suggestioni matematiche
  * *finestre* temporali
* Collages ed elaborazioni di frammenti
  * elaborazioni *music-centered*
  * elaborazioni *form-centered*

#### Spazio

* Lo spazio nei suoni
  * Distribuzione spaziale dei suoni (localizzazione)
  * Movimento dei suoni nello spazio (spazializzazione)
  * Concezione orchestrale
  * Concezione drammaturgica
  * Funzionamento delle tre dimensioni spaziali nell'ascolto
  * Lontano/Vicino
  * Presenza/Assenza
* Lo spazio fisico
* Lo spazio virtuale

### Tipologie compositive

#### Composizioni acusmatiche

* composizioni per configurazioni standard
* adattamento agli spazi fisici
* la dimensione concertistica
  * *acousmonium* - l'orchestra di altoparlanti
  * *l'esecuzione* di un brano acusmatico

#### Composizioni per strumenti e fixed media

* composizioni per strumento e fixed media propriamente dette
* composizioni per strumento e *dead-electronics* (fixed media interpretato)
* relazione tra strumento e fixed media
  * contrasto
  * mimesi
  * trasformazione

#### Composizioni per strumenti e live-electronics

* composizioni per strumento e *live-electronics* propriamente dette
* composizioni miste per strumento, *dead/live-electronics*
* relazione tra strumento e *live-electronics*
  * contrasto
  * mimesi
  * trasformazione

#### Composizioni per strumenti

* composizione assistita
* problematiche uomo-macchina

#### Altre forme compositive

* installazioni sonore
* *sound design*

#### Notazione della musica elettroacustica

* funzioni della notazione
  * notazione convenzionale
  * notazione grafica
  * tecnologie notazionali
* partiture d'ascolto
* partiture di realizzazione
* partiture d'esecuzione
* strutture delle partiture
  * legenda
  * schemi costruttivi
  * partiture generali
  * parti staccate

#### Ascolti analitici

* Schaeffer, *Étude aux chemin de fer*
* Chowning, *Stria*
* Harvey, *Mortuos plango, Vivum voco*
* Smalley, *Wind chimes*
* Razzi, *Progetto II*
* Risset, *Sud*
* Stockhausen, *Kontakte*
* Maderna, *Musica su Due Dimensioni*
