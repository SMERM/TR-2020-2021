# Composizione Musicale Elettroacustica (`COME-02`) - Prof.Nicola Bernardini
# Terza annualità (A.A.2022-2023)

## Macro-esercitazioni (Milestones)

### Attribuzione del punteggio degli esercizi

| Caratteristica            | Punteggio |
|---------------------------|-----------|
| Musicalità d'insieme      | + 3 punti |
| Realizzazione tecnica     | + 1 punto |
| Qualità sonora            | + 1 punto |
| Puntualità della consegna | + 1 punto |

### Esercizio 1: strumento e live electronics semplice (+ 6 punti)

* strumento dal vivo ed elaborazione semplice (ritardi/pitch shifters)
* utilizzare `pure data` o `supercollider` per l'elaborazione
* canali d'uscita: ≥ 2
* durata: 5-8 minuti
* data di consegna: entro domenica 22 gennaio 2023 ore 18
* brano di riferimento: [Luigi Nono, *Post-prae Ludium per Donau* (1987)](https://youtu.be/xMEh2aPncPM)

### Esercizio 2: strumento e live electronics sofisticato (+ 6 punti)

* strumento dal vivo ed elaborazione sofisticata (DNL/filtri)
* utilizzare `pure data` o `supercollider` per l'elaborazione
* canali d'uscita: ≥ 2
* durata: 5-8 minuti
* data di consegna: entro domenica 19 febbraio 2023 ore 18

### Esercizio 3: strumenti ed live-electronics incrociata (+ 6 punti)

* strumenti dal vivo e cross-control live 
* utilizzare `pure data` o `supercollider` per l'elaborazione
* canali d'uscita: 2
* durata: 5-8 minuti
* data di consegna: entro domenica 3 aprile 2023 ore 18

### Esercizio 4: voce e live-electronics (+ 6 punti)

* voce e live-electronics
* canali d'uscita: ≥ 2
* durata: 5-8 minuti
* data di consegna: entro domenica 7 maggio 2023 ore 18

### Esercizio 5: brano d'esame  (± 6 punti)

* brano per strumento e live electronics
* durata: 5-10 minuti
* data di consegna: data dell'appello d'esame

# Diario di Bordo

| Studente           | Es.1 | Es.2 | Es.3 | Es.4 | Esame | Voto |
|--------------------|:----:|:----:|:----:|:----:|:-----:|:----:|
| Ilaria Bava Dente  |      |      |      |      |       |      |
| Lorenzo Ceccarelli |      |      |      |      |       |      |
| Milo De Mattei     |      |      |      |      |       |      |
| Mauro Di Giovanni  |      |      |      |      |       |      |
| Federico Martino   |      |      |      |      |       |      |
| Kevin Miceli       |      |      |      |      |       |      |
| Leonardo Polla     |      |      |      |      |       |      |
| Leonardo Saba      |      |      |      |      |       |      |
