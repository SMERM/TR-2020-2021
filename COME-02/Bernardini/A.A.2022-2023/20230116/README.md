# Composizione Musicale Elettroacustica III (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 16/01/2023

### Argomenti

* Discussione dell'esercizio 1:
  * stato dell'arte
  * partiture
  * realizzazione
