# Composizione Musicale Elettroacustica III (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 21/11/2022

### Argomenti

* Discussione dell'esercizio 1:
  * scelta dello strumento
  * tecniche consentite:
    * retroazione
    * *bit-squashing* e *bit-crushing*
    
### Lavagne

[TR III 1](./TR_III_20221121.jpg)
