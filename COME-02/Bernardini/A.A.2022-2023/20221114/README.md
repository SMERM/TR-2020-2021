# Composizione Musicale Elettroacustica III (`COME-02`) - Prof.Nicola Bernardini
## Lezione del 14/11/2022

### Argomenti

* Discussione dell'esercizio 1:
  * scelta dello strumento
  * restrizioni tecniche:
    * linee di ritardo (single-tap, multi-tap, variabili ecc.)
    * pitch shifters (microtonali, macrotonali, problemi di traslazione spettrale, etc.)
    * combinazione dei due
  * approccio alla composizione
  * come affrontare la partitura:
    * partitura d'esecuzione
    * problemi di sostenibilità

### Lavagne

![TR III 1](./TR_III_20221114_1.jpg)
